FROM node:12-alpine AS build
WORKDIR /app
ARG FONTAWESOME_TOKEN
COPY package*.json ./
RUN \
    npm config set "@fortawesome:registry" https://npm.fontawesome.com/ && \
    npm config set "//npm.fontawesome.com/:_authToken" ${FONTAWESOME_TOKEN}
RUN npm install
COPY . .
ARG BUILD_ENV=dev
RUN if [ "${BUILD_ENV}" = "prod" ] ; then npm run build -- --prod --aot --configuration=production ; else npm run build ; fi
FROM nginx:stable
COPY --from=build /app/dist /var/www
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
