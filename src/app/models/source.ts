export class Source {
    id: number;                  // Primary key
    note: string;                // A note, e.g. 'Parliament website', max_length: 512
    url: string;                 // A URL, required, max_length: 350

    constructor(id?: number) {
        this.id = id;
        this.note = '';
        this.url = '';
    }
}

