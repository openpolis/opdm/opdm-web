import { Language } from './language';
import { Area } from './area';

export class AreaI18Name {
    area: Area;                  // required
    id: number;                  // Primary key
    language: Language;          // required
    name: string;                // required, max_length: 255

    constructor(id?: number) {
        this.id = id;
        this.area = <Area>{};
        this.language = <Language>{};
        this.name = '';
    }
}

