export class EducationLevel {
    id: number;                  // Primary key
    name: string;                // Education level name, required, max_length: 128

    constructor(id?: number) {
        this.id = id;
        this.name = '';
    }
}

