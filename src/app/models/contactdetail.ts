import { SourceRel } from './sourcerel';

export class ContactDetail {
    contact_type: string;        // A type of medium, e.g. 'fax' or 'email', required, max_length: 12
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    id: number;                  // Primary key
    label: string;               // A human-readable label for the contact detail, max_length: 256
    note: string;                // A note, e.g. for grouping contact details by physical location, max_length: 512
    object_id: number;           // required
    sources: SourceRel[];        // URLs to source documents about the contact detail, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    value: string;               // A value, e.g. a phone number or email address, required, max_length: 256

    constructor(id?: number) {
        this.id = id;
        this.contact_type = '';
        this.created_at = '';
        this.end_date = '';
        this.end_reason = '';
        this.label = '';
        this.note = '';
        this.object_id = null;
        this.sources = [];
        this.start_date = '';
        this.updated_at = '';
        this.value = '';
    }
}

