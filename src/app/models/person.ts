import { Identifier } from './identifier';
import { LinkRel } from './linkrel';
import { Profession } from './profession';
import { SourceRel } from './sourcerel';
import { EducationLevel } from './educationlevel';
import { ContactDetail } from './contactdetail';
import { OtherName } from './othername';
import { Area } from './area';
import {ClassificationRel} from './classificationrel';

export class Person {
    additional_name: string;     // One or more secondary given names, max_length: 128
    biography: string;           // An extended account of a person's life
    birth_date: string;          // A date of birth, max_length: 10
    birth_location: string;      // Birth location as a string, max_length: 128
    birth_location_area: Area;   // The geographic area corresponding to the birth location
    contact_details: ContactDetail[];    // Means of contacting the person, related model: ContactDetail
    classifications: ClassificationRel[];    // LABEL and all other available classifications, related model: ClassificationRel
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    death_date: string;          // A date of death, max_length: 10
    education_level: EducationLevel; // The education level of this person
    email: string;               // A preferred email address, max_length: 254
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    family_name: string;         // One or more family names, max_length: 128
    gender: string;              // A gender, max_length: 32
    given_name: string;          // One or more primary given names, max_length: 128
    honorific_prefix: string;    // One or more honorifics preceding a person's name, max_length: 32
    honorific_suffix: string;    // One or more honorifics following a person's name, max_length: 32
    id: number;                  // Primary key
    identifiers: Identifier[];   // Issued identifiers, related model: Identifier
    image: string;               // A URL of a head shot, max_length: 200
    links: LinkRel[];            // URLs to documents related to the person, related model: LinkRel
    name: string;                // A person's preferred full name, required, max_length: 512
    national_identity: string;   // A national identity, max_length: 128
    other_names: OtherName[];    // Alternate or former names, related model: OtherName
    patronymic_name: string;     // One or more patronymic names, max_length: 128
    profession: Profession;      // The profession of this person
    related_persons: Person[];   // required, related model: Person
    slug: string;                // required
    sort_name: string;           // A name to use in an lexicographically ordered list, max_length: 128
    sources: SourceRel[];        // URLs to source documents about the person, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    summary: string;             // A one-line account of a person's life, max_length: 1024
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    total_label: string;

    constructor(id: number = null) {
        this.id = id;
        this.additional_name = null;
        this.biography = null;
        this.birth_date = null;
        this.birth_location = null;
        this.birth_location_area = null;
        this.contact_details = [];
        this.classifications = [];
        this.created_at = null;
        this.death_date = null;
        // this.education_level = <EducationLevel>{};
        this.email = null;
        this.end_date = null;
        this.end_reason = null;
        this.family_name = null;
        this.gender = null;
        this.given_name = null;
        this.honorific_prefix = null;
        this.honorific_suffix = null;
        this.identifiers = [];
        this.image = null;
        this.links = [];
        this.name = null;
        this.national_identity = null;
        this.other_names = [];
        this.patronymic_name = null;
        // this.profession = <Profession>{};
        this.related_persons = [];
        this.slug = null;
        this.sort_name = null;
        this.sources = [];
        this.start_date = null;
        this.summary = null;
        this.updated_at = null;
        this.total_label = null;
    }
}

