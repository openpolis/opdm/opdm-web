export class Profession {
    id: number;                  // Primary key
    name: string;                // Profession name, required, max_length: 128

    constructor(id?: number) {
        this.id = id;
        this.name = '';
    }
}

