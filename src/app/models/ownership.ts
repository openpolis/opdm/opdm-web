import { SourceRel } from './sourcerel';
import { Organization } from './organization';
import { Person } from './person';

export class Ownership {
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    id: number;                  // Primary key
    organization: Organization;  // The organization that is owned, required
    owner_organization: Organization;    // An owner of the organization, when it is an Organization
    owner_person: Person;        // An owner of the organization, when it is a Person
    slug: string;                // required
    sources: SourceRel[];        // URLs to source documents about the ownership, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>

    constructor(id?: number) {
        this.id = id;
        this.created_at = '';
        this.end_date = '';
        this.end_reason = '';
        this.organization = <Organization>{};
        this.owner_organization = <Organization>{};
        this.owner_person = <Person>{};
        this.slug = '';
        this.sources = [];
        this.start_date = '';
        this.updated_at = '';
    }
}

