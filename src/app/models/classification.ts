import { SourceRel } from './sourcerel';

export class Classification {
    code: string;                // An alphanumerical code in use within the scheme, max_length: 128
    descr: string;               // The extended, textual description of the classification, max_length: 512
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    id: number;                  // Primary key
    parent: Classification;      // The parent classification.
    scheme: string;              // A classification scheme, e.g. ATECO, or FORMA_GIURIDICA, max_length: 128
    sources: SourceRel[];        // URLs to source documents about the classification, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10

    constructor(id?: number) {
        this.id = id;
        this.code = '';
        this.descr = '';
        this.end_date = '';
        this.end_reason = '';
        this.parent = <Classification>{};
        this.scheme = '';
        this.sources = [];
        this.start_date = '';
    }
}

