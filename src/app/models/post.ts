import { Organization } from './organization';
import { LinkRel } from './linkrel';
import { SourceRel } from './sourcerel';
import { ContactDetail } from './contactdetail';
import { Person } from './person';
import { Area } from './area';

export class Post {
    appointed_by: Post;          // The Post that officially appoints members to this one, ex: Secr. of Defence is appointed by POTUS
    area: Area;                  // The geographic area to which the post is related
    contact_details: ContactDetail[];    // Means of contacting the holder of the post, related model: ContactDetail
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    holders: Person[];           // required, related model: Person
    id: number;                  // Primary key
    label: string;               // A label describing the post, max_length: 256
    links: LinkRel[];            // URLs to documents about the post, related model: LinkRel
    organization: Organization;  // The organization in which the post is held
    other_label: string;         // An alternate label, such as an abbreviation, max_length: 32
    role: string;                // The function that the holder of the post fulfills, max_length: 512
    slug: string;                // required
    sources: SourceRel[];        // URLs to source documents about the post, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>

    constructor(id?: number) {
        this.id = id;
        this.appointed_by = <Post>{};
        this.area = <Area>{};
        this.contact_details = [];
        this.created_at = '';
        this.end_date = '';
        this.end_reason = '';
        this.holders = [];
        this.label = '';
        this.links = [];
        this.organization = <Organization>{};
        this.other_label = '';
        this.role = '';
        this.slug = '';
        this.sources = [];
        this.start_date = '';
        this.updated_at = '';
    }
}

