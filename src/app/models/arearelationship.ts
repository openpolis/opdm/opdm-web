import { SourceRel } from './sourcerel';
import { Area } from './area';

export class AreaRelationship {
    classification: string;      // The relationship classification, ex: Former ISTAT parent, ..., required, max_length: 3
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    dest_area: Area;             // The Area the relationship ends to, required
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    id: number;                  // Primary key
    note: string;                // Additional info about the relationship
    source_area: Area;           // The Area the relation starts from, required
    sources: SourceRel[];        // URLs to source documents about the relationship, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>

    constructor(id?: number) {
        this.id = id;
        this.classification = '';
        this.created_at = '';
        this.dest_area = <Area>{};
        this.end_date = '';
        this.end_reason = '';
        this.note = '';
        this.source_area = <Area>{};
        this.sources = [];
        this.start_date = '';
        this.updated_at = '';
    }
}

