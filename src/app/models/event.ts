import { Organization } from './organization';
import { Identifier } from './identifier';
import { SourceRel } from './sourcerel';
import { Person } from './person';
import { Area } from './area';

export class Event {
    area: Area;                  // The Area the Event is related to
    attendees: Person[];         // People attending the event, related model: Person
    classification: string;      // The event's category, max_length: 128
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    description: string;         // The event's description, max_length: 512
    end_date: string;            // The time at which the event ends, max_length: 20
    id: number;                  // Primary key
    identifiers: Identifier[];   // Issued identifiers for this event, related model: Identifier
    location: string;            // The event's location, max_length: 255
    name: string;                // The event's name, required, max_length: 128
    organization: Organization;  // The organization organizing the event
    parent: Event;               // The Event that this event is part of
    sources: SourceRel[];        // URLs to source documents about the event, related model: SourceRel
    start_date: string;          // The time at which the event starts, max_length: 20
    status: string;              // The event's status, max_length: 128
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>

    constructor(id?: number) {
        this.id = id;
        this.area = <Area>{};
        this.attendees = [];
        this.classification = '';
        this.created_at = '';
        this.description = '';
        this.end_date = '';
        this.identifiers = [];
        this.location = '';
        this.name = '';
        this.organization = <Organization>{};
        this.parent = <Event>{};
        this.sources = [];
        this.start_date = '';
        this.status = '';
        this.updated_at = '';
    }
}

