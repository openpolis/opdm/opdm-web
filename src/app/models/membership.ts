import { Organization } from './organization';
import { LinkRel } from './linkrel';
import { SourceRel } from './sourcerel';
import { ElectoralEvent } from './electoralevent';
import { Post } from './post';
import { ContactDetail } from './contactdetail';
import { Person } from './person';
import { Area } from './area';

export class Membership {
    area: Area;                  // The geographic area to which the membership is related
    constituency_descr_tmp: string;  // max_length: 64
    contact_details: ContactDetail[];    // Means of contacting the member of the organization, related model: ContactDetail
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    electoral_event: ElectoralEvent; // The electoral event that assigned this membership
    electoral_list_descr_tmp: string;    // max_length: 256
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    id: number;                  // Primary key
    label: string;               // A label describing the membership, max_length: 256
    links: LinkRel[];            // URLs to documents about the membership, related model: LinkRel
    member_organization: Organization;   // The organization who is a member of the organization
    on_behalf_of: Organization;  // The organization on whose behalf the person is a member of the organization
    appointed_by: Membership;    // The membership that appointed this one
    appointment_note: string;    // a note on the appointment
    organization: Organization;  // The organization in which the person or organization is a member
    person: Person;              // The person who is a member of the organization
    post: Post;                  // The post held by the person in the organization through this membership
    post_role?: string;
    role: string;                // The role that the member fulfills in the organization, max_length: 512
    slug: string;                // required
    sources: SourceRel[];        // URLs to source documents about the membership, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>

    constructor(initObject) {

        if (!initObject) {
            this.id = null;
            this.area = null;
            this.constituency_descr_tmp = '';
            this.contact_details = [];
            this.created_at = '';
            this.electoral_event = null;
            this.electoral_list_descr_tmp = '';
            this.end_date = '';
            this.end_reason = '';
            this.label = '';
            this.links = [];
            this.member_organization = null;
            this.on_behalf_of = null;
            this.appointed_by = <Membership>{};
            this.appointment_note = '';
            this.organization = null;
            this.person = null;
            this.post = null;
            this.role = null;
            this.slug = '';
            this.sources = [];
            this.start_date = '';
            this.updated_at = '';
        } else {

        }
    }

    public serialize() {
        this.post_role = this.post.role;
    }
}
