import { Organization } from './organization';
import { LinkRel } from './linkrel';
import { SourceRel } from './sourcerel';
import { ElectoralEvent } from './electoralevent';
import { Person } from './person';
import { Area } from './area';

export class ElectoralResult {
    candidate: Person;           // The candidate in the election these data are referred to
    constituency: Area;          // The electoral constituency these electoral data are referred to
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    event: ElectoralEvent;       // The generating electoral event, required
    id: number;                  // Primary key
    links: LinkRel[];            // URLs to documents referring to the electoral result, related model: LinkRel
    list: Organization;          // The electoral list these electoral data are referred to
    n_ballots: number;           // The total number of ballots casted
    n_eligible_voters: number;   // The total number of eligible voter
    n_preferences: number;       // The total number of preferences expressed for the list/candidate
    organization: Organization;  // The institution these electoral data are referred to, required
    slug: string;                // required
    sources: SourceRel[];        // URLs to sources about the electoral result, related model: SourceRel
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>

    constructor(id?: number) {
        this.id = id;
        this.candidate = <Person>{};
        this.constituency = <Area>{};
        this.created_at = '';
        this.event = <ElectoralEvent>{};
        this.links = [];
        this.list = <Organization>{};
        this.n_ballots = null;
        this.n_eligible_voters = null;
        this.n_preferences = null;
        this.organization = <Organization>{};
        this.slug = '';
        this.sources = [];
        this.updated_at = '';
    }
}

