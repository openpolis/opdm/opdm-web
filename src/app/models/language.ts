export class Language {
    dbpedia_resource: string;    // DbPedia URI of the resource, max_length: 255
    id: number;                  // Primary key
    iso639_1_code: string;       // ISO 639_1 code, ex: en, it, de, fr, es, ..., required, max_length: 2
    name: string;                // English name of the language, required, max_length: 128

    constructor(id?: number) {
        this.id = id;
        this.dbpedia_resource = '';
        this.iso639_1_code = '';
        this.name = '';
    }
}

