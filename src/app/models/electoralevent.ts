import { LinkRel } from './linkrel';
import { SourceRel } from './sourcerel';

export class ElectoralEvent {
    classification: string;      // An election classification, e.g. Regional, Municipal, required, max_length: 3
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    electoral_system: string;    // The electoral system under which this election session is held, required, max_length: 255
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    event_type: string;          // The electoral event type, e.g.: First round, run-off, required, max_length: 3, default: SIN
    id: number;                  // Primary key
    identifier: string;          // An issued identifier, max_length: 128
    links: LinkRel[];            // URLs to documents referring to the electoral event, related model: LinkRel
    name: string;                // A primary, generic name, e.g.: Local elections 2016, max_length: 256
    slug: string;                // required
    sources: SourceRel[];        // URLs to sources about the electoral event, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>

    constructor(id?: number) {
        this.id = id;
        this.classification = '';
        this.created_at = '';
        this.electoral_system = '';
        this.end_date = '';
        this.end_reason = '';
        this.event_type = 'SIN';
        this.identifier = '';
        this.links = [];
        this.name = '';
        this.slug = '';
        this.sources = [];
        this.start_date = '';
        this.updated_at = '';
    }
}

