import { Source } from './source';

export class SourceRel {
    id: number;                  // Primary key
    object_id: number;           // required
    source: Source;              // A Source instance assigned to this object, required

    constructor(id?: number) {
        this.id = id;
        this.object_id = null;
        this.source = <Source>{};
    }
}

