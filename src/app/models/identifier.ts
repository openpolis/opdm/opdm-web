export class Identifier {
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    id: number;                  // Primary key
    object_id: number;           // required
    scheme: string;              // An identifier scheme, e.g. DUNS, max_length: 128
    source: string;              // The URL of the source where this information comes from, max_length: 256
    start_date: string;          // The date when the validity of the item starts, max_length: 10

    constructor(id?: number) {
        this.id = id;
        this.end_date = '';
        this.end_reason = '';
        this.object_id = null;
        this.scheme = '';
        this.source = '';
        this.start_date = '';
    }
}

