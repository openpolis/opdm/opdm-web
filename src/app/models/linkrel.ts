import { Link } from './link';

export class LinkRel {
    id: number;                  // Primary key
    link: Link;                  // A relation to a Link instance assigned to this object, required
    object_id: number;           // required

    constructor(id?: number) {
        this.id = id;
        this.link = <Link>{};
        this.object_id = null;
    }
}

