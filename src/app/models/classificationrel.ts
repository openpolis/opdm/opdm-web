import { Classification } from './classification';

export class ClassificationRel {
    classification: Classification;  // A Classification instance assigned to this object, required
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    id: number;                  // Primary key
    object_id: number;           // required
    start_date: string;          // The date when the validity of the item starts, max_length: 10

    constructor(id?: number) {
        this.id = id;
        this.classification = <Classification>{};
        this.end_date = '';
        this.end_reason = '';
        this.object_id = null;
        this.start_date = '';
    }
}

