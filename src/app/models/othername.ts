export class OtherName {
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    id: number;                  // Primary key
    name: string;                // An alternate or former name, required, max_length: 512
    note: string;                // An extended note, e.g. 'Birth name used before marrige', max_length: 1024
    object_id: number;           // required
    othername_type: string;      // Type of other name, e.g. FOR: former, ALT: alternate, ..., required, max_length: 3, default: ALT
    source: string;              // The URL of the source where this information comes from, max_length: 256
    start_date: string;          // The date when the validity of the item starts, max_length: 10

    constructor(id?: number) {
        this.id = id;
        this.end_date = '';
        this.end_reason = '';
        this.name = '';
        this.note = '';
        this.object_id = null;
        this.othername_type = 'ALT';
        this.source = '';
        this.start_date = '';
    }
}

