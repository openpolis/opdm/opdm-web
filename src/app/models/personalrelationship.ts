import { SourceRel } from './sourcerel';
import { Person } from './person';

export class PersonalRelationship {
    classification: string;      // The relationship classification, ex: friendship, family, ..., required, max_length: 255
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    dest_person: Person;         // The Person the relationship ends to, required
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    id: number;                  // Primary key
    source_person: Person;       // The Person the relation starts from, required
    sources: SourceRel[];        // URLs to source documents about the relationship, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    weight: number;              // The relationship weight, from strongly negative, to strongly positive, required, default: 0

    constructor(id?: number) {
        this.id = id;
        this.classification = '';
        this.created_at = '';
        this.dest_person = <Person>{};
        this.end_date = '';
        this.end_reason = '';
        this.source_person = <Person>{};
        this.sources = [];
        this.start_date = '';
        this.updated_at = '';
        this.weight = 0;
    }
}

