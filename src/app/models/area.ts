import { LinkRel } from './linkrel';
import { SourceRel } from './sourcerel';
import { OtherName } from './othername';
import { Identifier } from './identifier';

export class Area {
    classification: string;      // An area category, according to GEONames definitions: http://www.geonames.org/export/codes.html, max_length: 128
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    geom: string;                // A geometry, expressed as text, eg: GeoJson, TopoJson, KML
    gps_lat: number;             // The Latitude, expressed as a float, eg: 85.3420
    gps_lon: number;             // The Longitude, expressed as a float, eg: 27.7172
    id: number;                  // Primary key
    identifier: string;          // The main issued identifier, max_length: 128
    identifiers: Identifier[];   // Other issued identifiers (zip code, other useful codes, ...), related model: Identifier
    inhabitants: number;         // The total number of inhabitants
    istat_classification: string;    // An area category, according to ISTAT: Ripartizione Geografica, Regione, Provincia, Città Metropolitana, Comune, max_length: 4
    links: LinkRel[];            // URLs to documents relted to the Area, related model: LinkRel
    name: string;                // The official, issued name, max_length: 256
    new_places: Area[];          // Link to area(s) after date_end, related model: Area
    other_names: OtherName[];    // Alternate or former names, related model: OtherName
    parent: Area;                // The area that contains this area, as for the main administrative subdivision.
    related_areas: Area[];       // Relationships between areas, required, related model: Area
    slug: string;                // required
    sources: SourceRel[];        // URLs to source documents about the Area, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>

    constructor(id?: number) {
        this.id = id;
        this.classification = '';
        this.created_at = '';
        this.end_date = '';
        this.end_reason = '';
        this.geom = '';
        this.gps_lat = null;
        this.gps_lon = null;
        this.identifier = '';
        this.identifiers = [];
        this.inhabitants = null;
        this.istat_classification = '';
        this.links = [];
        this.name = '';
        this.new_places = [];
        this.other_names = [];
        this.parent = <Area>{};
        this.related_areas = [];
        this.slug = '';
        this.sources = [];
        this.start_date = '';
        this.updated_at = '';
    }
}

