import { Identifier } from './identifier';
import { LinkRel } from './linkrel';
import { SourceRel } from './sourcerel';
import { ClassificationRel } from './classificationrel';
import { ContactDetail } from './contactdetail';
import { OtherName } from './othername';
import { Person } from './person';
import { Area } from './area';

export class Organization {
    abstract: string;            // A one-line description of an organization, max_length: 256
    area: Area;                  // The geographic area to which this organization is related
    classification: string;      // The nature of the organization, legal form in many cases, max_length: 256
    classifications: ClassificationRel[];    // ATECO, Legal Form and all other available classifications, related model: ClassificationRel
    contact_details: ContactDetail[];    // Means of contacting the organization, related model: ContactDetail
    created_at: string;          // required, default: <function now at 0x7f3a4d1269d8>
    description: string;         // An extended description of an organization
    dissolution_date: string;    // A date of dissolution, max_length: 10
    end_date: string;            // The date when the validity of the item ends, max_length: 10
    end_reason: string;          // The reason why the entity isn't valid any longer (eg: merge), max_length: 255
    founding_date: string;       // A date of founding, max_length: 10
    id: number;                  // Primary key
    identifier: string;          // The main issued identifier, or fiscal code, for organization, max_length: 32
    identifiers: Identifier[];   // Issued identifiers, related model: Identifier
    image: string;               // A URL of an image, to identify the organization visually, max_length: 255
    links: LinkRel[];            // URLs to documents about the organization, related model: LinkRel
    name: string;                // A primary name, e.g. a legally recognized name, required, max_length: 512
    new_orgs: Organization[];    // Link to organization(s) after dissolution_date, needed to track mergers, acquisition, splits., related model: Organization
    organization_members: Organization[];    // required, related model: Organization
    organization_owners: Organization[]; // required, related model: Organization
    other_names: OtherName[];    // Alternate or former names, related model: OtherName
    parent: Organization;        // The organization that contains this organization
    person_members: Person[];    // required, related model: Person
    person_owners: Person[];     // required, related model: Person
    slug: string;                // required
    sources: SourceRel[];        // URLs to source documents about the organization, related model: SourceRel
    start_date: string;          // The date when the validity of the item starts, max_length: 10
    thematic_classification: string; // What the organization does, in what fields, ..., max_length: 256
    updated_at: string;          // required, default: <function now at 0x7f3a4d1269d8>

    constructor(id?: number) {
        this.id = id;
        this.abstract = '';
        this.area = <Area>{};
        this.classification = '';
        this.classifications = [];
        this.contact_details = [];
        this.created_at = '';
        this.description = '';
        this.dissolution_date = '';
        this.end_date = '';
        this.end_reason = '';
        this.founding_date = '';
        this.identifier = '';
        this.identifiers = [];
        this.image = '';
        this.links = [];
        this.name = '';
        this.new_orgs = [];
        this.organization_members = [];
        this.organization_owners = [];
        this.other_names = [];
        this.parent = <Organization>{};
        this.person_members = [];
        this.person_owners = [];
        this.slug = '';
        this.sources = [];
        this.start_date = '';
        this.thematic_classification = '';
        this.updated_at = '';
    }
}

