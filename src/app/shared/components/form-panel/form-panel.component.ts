import {Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormlyFieldConfig, FormlyFormOptions} from '@ngx-formly/core';
import {FormGroup} from '@angular/forms';
import {takeWhile} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {APICommonService} from '../../../_services/api-common.service';
import {APISerializer} from '../../../serializers/generic';


@Component({
    selector: 'app-form-panel',
    template: `
        <form [formGroup]="form" (ngSubmit)="onSubmit(model)" class="ml-3 mr-5">
            <ng-container *ngIf="datasource; else formArray">
                <formly-form
                        *ngIf="isReady"
                        class="row"
                        [form]="form"
                        [fields]="fields.fieldGroup"
                        [model]="model"
                        (modelChange)="onModelChange($event)"
                        [options]="options"
                >
                </formly-form>
                <div class="clearfix mb-4">
                    <button [id]="datasource + '-panel-save-button'" type="button" class="btn btn-primary float-right panel-save-button"
                            (click)="form.valid && form.dirty && onSubmit(form.value)"
                            [disabled]="!form.valid || !form.dirty"
                            [ladda]="isSubmiting">Salva
                    </button>
                </div>
<!--                <div class="float-right mr-4">
                    <strong>dirty:</strong> {{form.dirty}} <strong>valid:</strong>{{form.valid}} <strong>touched:</strong>{{form.touched}}
                </div>-->
            </ng-container>

            <ng-template #formArray>
                <formly-field [field]="fields.fieldGroup[rowNum]"></formly-field>
            </ng-template>
        </form>
    `
})
export class FormPanelComponent implements OnInit, OnDestroy {

    @Input() model: any;
    @Input() fields: any;
    @Input() form = new FormGroup({});
    @Input() options: FormlyFormOptions = {
        formState: {
            model: {}
        }
    };
    @Input() rowNum = 0;
    @Input() datasource: any = null;
    @Input() datasourceParams: any;
    @Output() action_done: EventEmitter<any> = new EventEmitter<any>();
    @Output() action_error: EventEmitter<any> = new EventEmitter<any>();

    private alive = true;
    public isReady = false;
    public isSubmiting = false;

    // workaround for https://github.com/swimlane/ngx-datatable/issues/1141
    @HostListener('select', ['$event']) handleSelect(event: Event) {
        event.stopPropagation();
    }

    constructor(
        private apiService: APICommonService,
        private toastr: ToastrService,
    ) {
        // console.log(this.form);
    }

    ngOnInit() {
        // console.log('FormPanelComponent', this.model);
        // console.log('FormPanelComponent', this.model, this.fields);
        if (this.datasource && this.model.id) {
            // console.log('datasource', this.datasource);
            this.loadData();
        } else {
            this.isReady = true;
        }

    }

    ngOnDestroy() {
        // console.log('form destroy');
        this.alive = false;
    }

    onModelChange($event) {
        // console.log('onModelChange', $event);
        // this.model = $event;
    }

    private loadData() {

        this.apiService.getById(this.datasource, this.model.id)
            .pipe(takeWhile(() => this.alive))
            .subscribe(
                (res: any) => {
                    // console.log('datasource results', res);
                    // this.form = new FormGroup({});
                    this.model = res;
                    this.isReady = true;
                }
            );
    }

    public onSubmit(data) {

        const serializer = new APISerializer(this.datasource, this.model);
        const submitData = serializer.serialize();

        if (submitData.id) {
            this.apiService.update(this.datasource, submitData).subscribe(
                response => {
                    this.form.markAsPristine();
                    this.toastr.success(null, 'Salvataggio OK');
                    this.action_done.emit({do: 'reload'})
                }
            );
        } else {
            this.apiService.create(this.datasource, submitData).subscribe(
                response => {
                    this.form.markAsPristine();
                    this.toastr.success(null, 'Salvataggio OK');
                    this.action_done.emit({do: 'reload'})
                }
            )
        }

        // console.log(data);
        // console.log(this.model);
    }
}
