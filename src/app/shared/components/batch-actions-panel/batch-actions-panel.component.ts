import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';


import {debounceTime, takeWhile} from 'rxjs/operators';

import {FormlyFieldConfig} from '@ngx-formly/core';

@Component({
  selector: 'app-batch-actions-panel',
  templateUrl: './batch-actions-panel.component.html',
})
export class BatchActionsPanelComponent implements OnInit, OnDestroy {

  private alive = true;
  public _model = {};

  @Input() fields: FormlyFieldConfig[] = [];
  @Input() selected = [];
  @Input() total: number;
  @Input() form: FormGroup;
  @Output() actionchange: EventEmitter<any> = new EventEmitter<any>();
  @Output() actionsubmit: EventEmitter<any> = new EventEmitter<any>();

  @Input() set model(value: any) {
    Object.assign(this._model, value);
  };

  public ngOnInit() {

    // console.log('ActionsPanelComponent', ...this.fields);

    this.form.valueChanges
      .pipe(
        takeWhile(() => this.alive),
        debounceTime(1000)
      )
      .subscribe(data => {
        this.actionchange.emit(data);
      });
  }

  public applyChanges($event) {
    const D: any = {};

    if (Array.isArray(this.fields)) {
      this.fields.forEach(field => {
        if (field.formControl && field.formControl['controls']) {

          Object.keys(field.formControl['controls']).map(key => {
            D[key] = this._model[key];
          });
        }
      })
    }
    delete D.action;
    this.actionsubmit.emit(D);
  }

  public ngOnDestroy() {
    this.alive = false;
  }
}
