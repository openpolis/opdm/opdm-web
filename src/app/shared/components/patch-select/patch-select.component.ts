import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {APICommonService} from '../../../_services/api-common.service';
import {takeWhile} from 'rxjs/internal/operators';

@Component({
  selector: 'app-patchselect',
  template: `
    <ng-select class="form-control p-0"
               [items]="items"
               bindLabel="name"
               bindValue="id"
               appendTo="body"
               (ngModelChange)="saveItem($event)"
               [multiple]="options.multiple || false"
               [(ngModel)]="model">
    </ng-select>
    <!--<pre>{{value | json}}</pre>-->
    `,
})
export class PatchSelectComponent implements OnInit, OnDestroy {
  @Input() value;
  @Input() options: any = {};
  @Input() static_data;
  @Input() saveId;
  @Input() column_definition;

  items:  Array<any> = null;

  public model = null;
  private alive = true;

  constructor(
    private apiService: APICommonService
  ) { }

  ngOnInit(): void {
    if (this.options.multiple && Array.isArray(this.value)) {
      this.model = this.value.map(item => item.id);
    } else {
      this.model = this.value.id;
    }

    this.static_data[this.options.static_data_prop].subscribe(res => {
      this.items = res.results;
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }

  saveItem($event) {

    const data = {};
    data[this.column_definition.prop] = $event;
    if (this.saveId) {
      this.apiService.patch(this.options.save_path, data, this.saveId).subscribe(res => {
        console.log('patch', res);
      })
    }
  }
}
