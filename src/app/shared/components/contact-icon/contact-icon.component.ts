import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-contact-icon',
  templateUrl: './contact-icon.component.html'
})
export class ContactIconComponent {
  @Input() contact: any;
  @Input() showdetail = false;
  @Input() showinline = true;
}
