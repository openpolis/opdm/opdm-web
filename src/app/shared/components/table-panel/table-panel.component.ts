// ANGULAR MODULES
import {Component, Input, OnDestroy, OnInit} from '@angular/core';

// RXJS MODULES
import {takeWhile} from 'rxjs/operators';

// APP SERVICES
import {APICommonService} from '../../../_services/api-common.service';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-table-panel',
    template: `
        <ngx-datatable
                class="material grey-header border-bottom animated fadeIn mr-3"
                style="height: 500px"
                [columns]="fields"
                [rows]="rows"
                [columnMode]="'flex'"
                [headerHeight]="42"
                [footerHeight]="50"
                [scrollbarV]="true"
                [virtualization]="false"
                [rowHeight]="thisGetRowHeight"
                [messages]="{ emptyMessage: emptyMessage, totalMessage:	'risultati' }"
        >
        </ngx-datatable>
    `
})
export class TablePanelComponent implements OnInit, OnDestroy {
    @Input() model: any;
    @Input() fields: Array<any>;
    @Input() rowHeight: any;
    @Input() datasource: any;
    @Input() datasourceParams: any;

    public emptyMessage = 'Caricamento dati';

    public rows: Array<any>;
    private alive = true;

    // workaround for ngx-datatable calling rowHeight function on anonymous context
    public thisGetRowHeight = this.getRowHeight.bind(this);

    constructor(
        private apiService: APICommonService,
        private toastr: ToastrService
    ) {
    }

    ngOnInit() {
        if (typeof this.datasource === 'function') {
          this.datasource = this.datasource(this.model);
        }
        if (typeof this.datasourceParams === 'function') {
          this.datasourceParams = this.datasourceParams(this.model);
        }

        this.apiService.getByParams(this.datasource, this.datasourceParams)
            .pipe(takeWhile(() => this.alive))
            .subscribe(
                (res: any) => {
                    if (res.count === 0) {
                      this.emptyMessage = 'Nessun risultato';
                    }
                    const parent_id = this.model.id;
                    const is_resolved = this.model.is_resolved;
                    this.rows = res.results.map((item) => {
                        item.parent_id = parent_id;
                        item.is_resolved = is_resolved;
                        return item;
                    });
                    // this.rows = res.results;
                },
                (err) => {
                    this.toastr.error(err.message, 'Errore ' + err.status, {timeOut: 10000});
                    console.log('Error occured', err);
                }
            );
    }

    ngOnDestroy() {
        this.alive = false;
    }

    getRowHeight(row) {
        if (this.rowHeight) {
            if (typeof this.rowHeight === 'function') {
                return this.rowHeight(row) || 50;
            } else {
                return this.rowHeight;
            }
        }

        return 50;
    }

}
