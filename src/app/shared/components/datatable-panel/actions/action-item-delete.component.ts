// ANGULAR MODULES
import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, TemplateRef} from '@angular/core';

// VENDOR
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

// RXJS MODULES
import {takeWhile} from 'rxjs/internal/operators';

// APP SERVICES
import {APICommonService} from '../../../../_services/api-common.service';

@Component({
    selector: 'app-action-item-delete',
    template: `
        <ng-container *ngIf="!apipath || data.id">
            <span class="mr-3 clickable" title="Elimina" (click)="deleteAsk(confirmTemplate)">
                <i class="far fa-trash-alt"></i>
            </span>
        </ng-container>
        <ng-template #confirmTemplate>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Conferma rimozione</h5>
                </div>
                <div class="modal-body">
                    <p>Sei sicuro di voler eliminare questo elemento ?</p>
                    <hr>
                    <p *ngIf="apipath">API Endpoint: <strong>{{apipath}}</strong></p>
                    <ng-container *ngIf="data.total_label; else dataDump;">
                        <h5>{{data.total_label}}</h5>
                    </ng-container>
                    <ng-template #dataDump>
                        <pre style="max-height: 400px; overflow: auto;">{{data | json}}</pre>
                    </ng-template>
                    <div class="alert alert-danger mt-4" role="alert">
                        <strong>Attenzione:</strong> potrebbero essere rimossi anche eventuali dati collegati
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="item-delete-decline-button" type="button" class="btn btn-secondary" (click)="decline()">Annulla</button>
                    <button id="item-delete-confirm-button" type="button" class="btn btn-danger" (click)="confirm()">Conferma</button>
                </div>
            </div>
        </ng-template>
    `
})
export class ActionItemDeleteComponent implements OnInit, OnDestroy {
    @Input() data: any;
    @Input() apipath: string;
    @Input() rowIndex: number;
    @Output() action: EventEmitter<any> = new EventEmitter<any>();
    @Output() error: EventEmitter<any> = new EventEmitter<any>();

    private id;
    private modalRef: BsModalRef;
    private alive = true;

    constructor(
        private apiService: APICommonService,
        private modalService: BsModalService
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.alive = false;
    }

    deleteAsk(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'modal-lg modal-dialog-centered' })
        );
    }

    public confirm() {
        this.modalRef.hide();
        this.delete(false);
    }

    public decline() {
        this.modalRef.hide();
    }

    public delete(edit: boolean) {

        let actionDone = {};

        if (this.apipath) {
            actionDone = { do: 'delete', doParams: this.data.id, apipath: this.apipath };
        } else {
            actionDone = { do: 'delete', doParams: this.rowIndex, apipath: null };
        }

        this.action.emit(actionDone);
    }
}
