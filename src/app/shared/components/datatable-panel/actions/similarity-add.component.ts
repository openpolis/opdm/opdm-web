// ANGULAR MODULES
import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

// RXJS MODULES
import {takeWhile} from 'rxjs/operators';

// APP SERVICES
import {APICommonService} from '../../../../_services/api-common.service';

@Component({
    selector: 'app-similarity-add',
    template: `
        <ng-container *ngIf="!is_resolved; else resolved;">
            <span class="fa-layers mr-3 clickable" title="Crea nuova persona" (click)="createNew(false)">
                <i class="fas fa-user"></i>
                <i class="fas fa-plus" data-fa-transform="shrink-7 right-10 up-6"></i>
            </span>
            <span class="fa-layers clickable" title="Crea nuova persona e modifica" (click)="createNew(true)">
                <i class="fas fa-user-edit"></i>
                <i class="fas fa-plus" data-fa-transform="shrink-7 right-10 up-6"></i>
            </span>
        </ng-container>
        <ng-template #resolved>
<!--            <span class="fa-layers mr-3 clickable" title="Elimina risoluzione" (click)="removeBind()">
                <i class="fas fa-user"></i>
                <i class="fas fa-minus" data-fa-transform="shrink-7 right-10 up-6"></i>
            </span>-->
        </ng-template>
    `
})
export class SimilarityAddComponent implements OnInit, OnDestroy {
    @Input() id: any;
    @Input() is_resolved: boolean;
    @Input() apipath: string;
    @Output() action: EventEmitter<any> = new EventEmitter<any>();
    @Output() error: EventEmitter<any> = new EventEmitter<any>();

    private alive = true;

    constructor(
        private apiService: APICommonService
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.alive = false;
    }

    public createNew(edit: boolean) {
        const data = {
            is_resolved: true
        };
        this.apiService.patch(this.apipath, data, this.id)
            .pipe(takeWhile(() => this.alive))
            .subscribe(
                (response: any) => {
                    const actionDone = {
                        do: edit ? 'navigate' : 'reload',
                        doParams: edit ? {url: '/persons/edit/' + response.object_id} : null,
                        data: response
                    };

                    this.action.emit(actionDone);
                },
                (err) => {
                    console.log('Error occured', err);
                    this.error.emit(err);
                }
            );
    }

    public removeBind() {
        const data = {
            is_resolved: false
        };
        this.apiService.patch(this.apipath, data, this.id)
            .pipe(takeWhile(() => this.alive))
            .subscribe(
                (response: any) => {
                    const actionDone = {
                        do: 'reload',
                        doParams: null,
                        data: response
                    };

                    this.action.emit(actionDone);
                },
                (err) => {
                    console.log('Error occured', err);
                    this.error.emit(err);
                }
            );
    }
}
