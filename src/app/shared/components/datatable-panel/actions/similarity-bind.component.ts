// ANGULAR MODULES
import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';

// RXJS MODULES
import {takeWhile} from 'rxjs/operators';

// APP SERVICES
import {APICommonService} from '../../../../_services/api-common.service';

@Component({
    selector: 'app-similarity-bind',
    template: `
        <ng-container *ngIf="!is_resolved;">
            <span class="fa-layers mr-3 clickable" title="Associa a questa persona" (click)="bindTo(false)">
                <i class="fas fa-user"></i>
                <i class="fas fa-check" data-fa-transform="shrink-7 right-9 up-6"></i>
            </span>
            <span class="fa-layers clickable" title="Associa a questa persona e modifica" (click)="bindTo(true)">
                <i class="fas fa-user-edit"></i>
                <i class="fas fa-check" data-fa-transform="shrink-7 right-9 up-6"></i>
            </span>
        </ng-container>
    `
})
export class SimilarityBindComponent implements OnInit, OnDestroy {
    @Input() id: any;
    @Input() is_resolved: boolean;
    @Input() parent_id: any;
    @Input() apipath: string;
    @Output() action: EventEmitter<any> = new EventEmitter<any>();
    @Output() error: EventEmitter<any> = new EventEmitter<any>();

    private alive = true;

    constructor(
        private apiService: APICommonService
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.alive = false;
    }

    public bindTo(edit: boolean) {
        const data = {
            is_resolved: true,
            object_id: this.id
        };
        this.apiService.patch(this.apipath, data, this.parent_id)
            .pipe(takeWhile(() => this.alive))
            .subscribe(
                (response: any) => {
                    const actionDone = {
                        do: edit ? 'navigate' : 'reload',
                        doParams: edit ? {url: '/persons/edit/' + this.id} : null,
                        data: response
                    };

                    this.action.emit(actionDone);
                },
                (err) => {
                    this.error.emit(err);
                }
            );
    }
}
