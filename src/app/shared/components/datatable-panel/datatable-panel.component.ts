import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import intersectionBy from 'lodash-es/intersectionBy';
import {takeWhile} from '../../../../../node_modules/rxjs/internal/operators';
import {APICommonService} from '../../../_services/api-common.service';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {FormGroup} from '@angular/forms';
import {FormlyFormOptions} from '@ngx-formly/core';

@Component({
  selector: 'app-datatable-panel',
  templateUrl: './datatable-panel.component.html',
  styles: []
})
export class DatatablePanelComponent implements OnInit {
  @ViewChild(DatatableComponent, {static: true}) table: DatatableComponent;

  /* TABLE HEADERS DEFINITION BINDINGS */
  @ViewChild('T_hdr', {static: true}) T_hdr: TemplateRef<any>;
  @ViewChild('T_hdr_sel', {static: true}) T_hdr_sel: TemplateRef<any>;
  @ViewChild('T_hdr_exp', {static: true}) T_hdr_exp: TemplateRef<any>;

  /* TABLE CELLS DEFINITION BINDINGS */
  @ViewChild('T_exp', {static: true}) T_exp: TemplateRef<any>;
  @ViewChild('T_exp_badge', {static: true}) T_exp_badge: TemplateRef<any>;
  @ViewChild('T_id', {static: true}) T_id: TemplateRef<any>;
  @ViewChild('T_ddMMyyyy', {static: true}) T_ddMMyyyy: TemplateRef<any>;
  @ViewChild('T_ddMMyyyyHHmmss', {static: true}) T_ddMMyyyyHHmmss: TemplateRef<any>;
  @ViewChild('T_identifiers', {static: true}) T_identifiers: TemplateRef<any>;
  @ViewChild('T_gender', {static: true}) T_gender: TemplateRef<any>;
  @ViewChild('T_contact_details', {static: true}) T_contact_details: TemplateRef<any>;
  @ViewChild('T_personlink', {static: true}) T_personlink: TemplateRef<any>;
  @ViewChild('T_person', {static: true}) T_person: TemplateRef<any>;
  @ViewChild('T_memberperson', {static: true}) T_memberperson: TemplateRef<any>;
  @ViewChild('T_organization', {static: true}) T_organization: TemplateRef<any>;
  @ViewChild('T_membership', {static: true}) T_membership: TemplateRef<any>;
  @ViewChild('T_memberships', {static: true}) T_memberships: TemplateRef<any>;
  @ViewChild('T_simil_add', {static: true}) T_simil_add: TemplateRef<any>;
  @ViewChild('T_simil_bind', {static: true}) T_simil_bind: TemplateRef<any>;
  @ViewChild('T_sep_num', {static: true}) T_sep_num: TemplateRef<any>;
  @ViewChild('T_text', {static: true}) T_text: TemplateRef<any>;
  @ViewChild('T_json', {static: true}) T_json: TemplateRef<any>;
  @ViewChild('T_patchselect', {static: true}) T_patchselect: TemplateRef<any>;
  @ViewChild('T_org_members', {static: true}) T_org_members: TemplateRef<any>;
  @ViewChild('T_empty', {static: true}) T_empty: TemplateRef<any>;
  @ViewChild('T_delete', {static: true}) T_delete: TemplateRef<any>;


  // empty Templates holders (defined OnInit as @ViewChild references are not available before)
  private headerTemplates: {} = null;
  private cellTemplates: {} = null;

  @Input() page_size = 0;
  @Input() externalPaging = true;
  @Input() apipath;
  @Input() title;
  @Input() queryParams;
  @Input() rows_count = 0;
  @Input() form;
  @Input() field;
  @Input() static_data;

  // ROWS
  public _rows = [];
  @Input() set rows(value: any[]) {
    // console.log('set rows', value);
    this._rows = value;

    // refresh the rows counter
    if (Array.isArray(this._rows) && this._rows.length > this.rows_count) {
      this.rows_count = this._rows.length;
    }

    this.onSelect({selected: []});

    // delays an action to restore the previous detail row opening status
    setTimeout(() => {
      this.restoreDetailOpenStatus()
    }, 500);
  };

  // COLUMNS
  private _rawColumns: any[] = null;
  public _columns: any[];
  @Input() set columns(value: any[]) {
    if (this.headerTemplates && this.cellTemplates) {
      this._columns = this.prepareColumns(value);
      this.allColumns = [...this._columns];
    } else {
      this._rawColumns = value;
    }
  };

  // TEMPLATE OPTIONS
  private _rawOptions: any;
  public _TPLOptions: any = {};

  @Input() set TPLoptions(value: any) {
    // TODO better typing: options belongs to ListTPLOptions
    if (this.headerTemplates && this.cellTemplates) {
      this._TPLOptions = this.prepareOptions(value);
    } else {
      this._rawOptions = value;
    }
  }

  @Input() options: FormlyFormOptions;

  @Input() set expandRow(row: number) {
    // console.log('set expandRow', row);
    this.rowsDetailOpen = row;
  }

  private alive = true;
  public ready = false;


  @Input() offset_current = 0;
  public isLoading = true;

  public rowsDetailOpen: number = null;
  public expandAll = false;

  public allColumns = [];

  public selected = [];

  @Output() ordering: EventEmitter<any> = new EventEmitter<any>();
  @Output() selection: EventEmitter<any> = new EventEmitter<any>();
  @Output() paging: EventEmitter<any> = new EventEmitter<any>();

  @Output() action_done: EventEmitter<any> = new EventEmitter<any>();
  @Output() action_error: EventEmitter<any> = new EventEmitter<any>();

  public detailRowModel: any;

  public allowAddRow = false;

  constructor(
    private apiService: APICommonService,
    private cdRef: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.initTemplates();
    if (this._rawOptions) {
      this._TPLOptions = this.prepareOptions(this._rawOptions);
    }
    if (this._rawColumns) {
      this._columns = this.prepareColumns(this._rawColumns);
      this.allColumns = [...this._columns];
    }
  }

  // Includes
  private initTemplates() {
    // console.log('INIT TEMPLATES');
    this.headerTemplates = {
      hdr: this.T_hdr,
      hdr_sel: this.T_hdr_sel,
      hdr_exp: this.T_hdr_exp
    };

    this.cellTemplates = {
      exp: this.T_exp,
      exp_badge: this.T_exp_badge,
      id: this.T_id,
      ddMMyyyy: this.T_ddMMyyyy,
      ddMMyyyyHHmmss: this.T_ddMMyyyyHHmmss,
      identifiers: this.T_identifiers,
      gender: this.T_gender,
      contact_details: this.T_contact_details,
      personlink: this.T_personlink,
      person: this.T_person,
      memberperson: this.T_memberperson,
      organization: this.T_organization,
      membership: this.T_membership,
      memberships: this.T_memberships,
      simil_add: this.T_simil_add,
      simil_bind: this.T_simil_bind,
      sep_num: this.T_sep_num,
      text: this.T_text,
      json: this.T_json,
      patchselect: this.T_patchselect,
      org_members: this.T_org_members,
      empty: this.T_empty,
      delete: this.T_delete
    };
  }

  public onSelect($event) {
    this.selected = $event.selected;
    // console.log('onSelect', $event, this.selected);

    this.selection.emit(this.selected);
  }


  private restoreDetailOpenStatus() {
    if (this.rowsDetailOpen !== null) {
      this.table.rowDetail.toggleExpandRow(this._rows[this.rowsDetailOpen]);
    }
  }

  public onSort($event) {
    // console.log('onSort', $event);

    if ($event && $event.sorts && $event.sorts[0]) {
      this.ordering.emit($event);
    }
  }

  public setPage($event) {
    this.paging.emit($event);
  }

  /* TABLE COLUMN TOGGLE */
  public toggleColumn(col) {
    const isChecked = this.isColumnChecked(col);

    if (isChecked) {
      this._columns = this._columns.filter(c => {
        return c.name !== col.name;
      });
    } else {

      // append newly selected column
      this._columns = [...this._columns, col];
      // retain original sequence of columns by comparing to allColumns array;
      // this means allColumns manages state of desired sequence
      this._columns = intersectionBy(this.allColumns, this._columns, 'name');

      // console.log(this._columns);
    }
  }

  public isColumnChecked(col) {
    return this._columns.find(c => {
      return c.name === col.name;
    });
  }

  public onDetailToggle(event) {
    // console.log('DatatablePanelComponent->onDetailToggle', event);
    // this.detailRowModel = event.value;
  }

  /* TABLE ROW DETAIL TOGGLE */
  public toggleExpandRow(row) {

    this.detailRowModel = row;

    const rowIndex = this._rows.findIndex((item) => {

      // if (item === row) { console.log('same obj found'); }
      return item === row;
      // return item.id === row.id;
    });
    // console.log('toggle', rowIndex, row);

    if (this.rowsDetailOpen === null) {
      // nothing is already opened
      // console.log('// nothing is already opened');
      this.rowsDetailOpen = rowIndex;
      this.table.rowDetail.toggleExpandRow(row);
    } else if (this.rowsDetailOpen === rowIndex) {
      // the same is already opened
      // console.log('// the same is already opened');
      this.rowsDetailOpen = null;
      this.table.rowDetail.toggleExpandRow(row);
    } else {
      // another is already opened
      // console.log('// another is already opened');
      this.table.rowDetail.collapseAllRows();
      /*            setTimeout(() => {
                      this.table.rowDetail.toggleExpandRow(row);
                      this.rowsDetailOpen = rowIndex;
                  }, 10);*/

      this.table.rowDetail.toggleExpandRow(row);
      this.rowsDetailOpen = rowIndex;
    }

    // this.cdRef.markForCheck();

    return false;
  }

  public toggleExpandAll() {
    if (!this.expandAll) {
      this.table.rowDetail.expandAllRows();
    } else {
      this.table.rowDetail.collapseAllRows();
    }
    this.expandAll = !this.expandAll;
  }

  public onActionDone($event: any) {
    this.action_done.emit($event);
  }

  public onActionError($event: any) {
    this.action_error.emit($event);
    console.log('Error occured', $event);
  }

  private prepareColumns(columns: Array<any>): Array<any> {
    // console.log('PREPARE COLUMNS');
    columns.forEach(col => this.parseOptions(col));

    // console.log(columns);
    return columns;
  }

  private prepareOptions(options) {

    if (options.row_detail) {

      if (Array.isArray(options.row_detail.fields)) {

        switch (options.row_detail.type) {
          case 'form':

            break;
          case 'table':
            options.row_detail.fields.forEach(col => this.parseOptions(col));
            break;
          default:
            console.log('unsupported row_detail.type');

        }
      }
    }

    return options;
  }

  private parseOptions(col) {

    if (typeof col.head === 'string') {
      col.headerTemplate = this.headerTemplates[col.head];
    }

    if (typeof col.cell === 'string') {
      col.cellTemplate = this.cellTemplates[col.cell];
    }

    col.resizeable = col.resize || 0;
    col.sortable = col.sort || 0;
    col.draggable = col.drag || 0;
  }

  public getRowIndex(row): number {
    const rowIndex = this._rows.findIndex((item) => {
      // if (item === row) { console.log('same obj found'); }
      return item === row;
      // return item.id === row.id;
    });

    return rowIndex;
  }

  public getField(column, row) {
    const rowIndex = this.getRowIndex(row);
    console.log(rowIndex);
    return this.field.fieldGroup[rowIndex].fieldGroup.find(f => f.key === column.prop);
  }

  public spreadRow(row) {
    return {...row};
  }

  public log(column: any) {
    console.log(column);
  }
}
