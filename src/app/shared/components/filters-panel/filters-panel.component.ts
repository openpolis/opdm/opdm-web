import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';


import {debounceTime, takeWhile} from 'rxjs/operators';

import {FormlyFieldConfig} from '@ngx-formly/core';

@Component({
  selector: 'app-filters-panel',
  templateUrl: './filters-panel.component.html',
})
export class FiltersPanelComponent implements OnInit, OnDestroy {

  private alive = true;

  public _model: any = {};
  @Output() filterchange: EventEmitter<any> = new EventEmitter<any>();
  @Input() fields: FormlyFieldConfig[] = [];
  @Input() form: FormGroup;
  @Input() set model(value: any) {
    // this._model = value;
    Object.assign(this._model, value);
/*    setTimeout(() => {
      this._model = {...this._model};
    }, 1);*/
  };

  public ngOnInit() {

    // console.log('FiltersPanelComponent', ...this.fields);

    this.form.valueChanges
      .pipe(
        takeWhile(() => this.alive),
        debounceTime(1000)
      )
      .subscribe(data => {
        // console.log('valueChanges', data);
        this.filterchange.emit(data);
      });
  }

  public ngOnDestroy() {
    this.alive = false;
  }
}
