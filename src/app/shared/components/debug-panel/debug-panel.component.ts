import { Component, OnInit, Input } from '@angular/core';

// APP CONFIG
import {AppConfig} from '../../../app.config';

@Component({
  selector: 'app-debug-panel',
  templateUrl: './debug-panel.component.html'
})
export class DebugPanelComponent implements OnInit {
  @Input() model;
  @Input() label;

  constructor(
      public config: AppConfig
  ) { }

  ngOnInit() {
  }

}
