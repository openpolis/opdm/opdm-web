import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-identifier-icon',
  templateUrl: './identifier-icon.component.html'
})
export class IdentifierIconComponent {
  @Input() id: any;
  @Input() showdetail = false;
  @Input() showinline = true;
}
