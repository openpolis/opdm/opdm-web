import {AfterViewInit, ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import { FieldArrayType } from '@ngx-formly/core';

import {FormlyFieldConfig} from '@ngx-formly/core';
import {takeWhile} from 'rxjs/operators';
import {APICommonService} from '../../../_services/api-common.service';
import {ToastrService} from 'ngx-toastr';
import {FormGroup} from '@angular/forms';

@Component({
    selector: 'app-formly-repeat-section',
    template: `
        <div *ngIf="isLoading">Caricamento dati <i class="fas fa-circle-notch fa-spin text-palette-4"></i></div>
        <app-datatable-panel
                *ngIf="!isLoading"
                [rows]="rows"
                [rows_count]="rows_count"
                [columns]="columns"
                [page_size]="filterQuery.page_size"
                [offset_current]="0"
                [TPLoptions]="to"
                [expandRow]="expandRow"
                [form]="subForm"
                [field]="subField"
                [formlyAttributes]="field"
                [options]="options"
                [apipath]="apipath"
                [externalPaging]="externalPaging"
                (paging)="setPage($event)"
                (action_done)="onActionDone($event)"
                (action_error)="onActionError($event)"></app-datatable-panel>
        <div class="row-add" *ngIf="allowAddRow">
            <button [id]="apipath + '-add-row'" class="btn btn-sm btn-outline-success" type="button" (click)="addRow()"><i class="fas fa-plus"></i></button>
        </div>
    `
})
export class FormlyDatatablePanelComponent extends FieldArrayType implements OnInit, AfterViewInit, OnDestroy {


    public opts = [];
    // public model = [];
    public rows = [];
    public rows_count = 0;

    private alive = true;
    public isLoading = true;
    public apipath: string;
    public datasource: string;
    public externalPaging = true;

    public allowAddRow = false;
    public expandRow: number = null;

    public filterQuery: any = {
        page_size: 15,
        page: 1
    };

    public subForm: any = null;
    public subField: any = null;

    private resizeTimeout = null;
    @HostListener('window:resize', ['$event']) onResize(event: any) {
        // workaround for detail row not resizing on window resize
        // https://github.com/swimlane/ngx-datatable/issues/1355
        clearTimeout(this.resizeTimeout);
        this.resizeTimeout = setTimeout(() => {
            try {
                // console.log(this);
                if (this.to && Array.isArray(this.to.columns)) {
                    this.to.columns = [...this.to.columns];
                } else if (event) {
                    console.log('FormlyDatatablePanelComponent -> rescheduling onresize');
                    this.onResize(false);
                }
            } catch (err) {
                console.log('FormlyDatatablePanelComponent -> this.to undefined, why?');
            }

        }, 100);
    }

    get columns() {
        return this.to.columns;
    }

    constructor(
        private apiService: APICommonService,
        private cdRef: ChangeDetectorRef,
        private toastr: ToastrService
    ) {
        super();
    }

    ngOnInit() {
        // console.log('FormlyDatatablePanelComponent', this.form);
        // console.log(this.field.fieldGroup);

        if (this.field.templateOptions.datasource && this.field.templateOptions.datasource !== null) {
            this.datasource = this.field.templateOptions.datasource;
            this.apipath = this.field.templateOptions.datasource.split('?')[0];
            this.externalPaging = true;

            this.subForm = new FormGroup({});
            this.subField = this.field.fieldArray;
            this.loadData(this.datasource);
        } else {

            this.rows = this.model;
            this.rows_count = this.model.length;
            this.externalPaging = false;
            this.subForm = this.form;
            this.subField = this.field;
            this.isLoading = false;
        }

        if (this.field.key === 'ACTIONBUS') {
            this.apiService.actionsBus.pipe(
                takeWhile(() => this.alive)
            ).subscribe(action => {

                switch (action.type) {
                    case 'similarity_check':
                        // console.log(action);
                        // console.log('similarity_check');
                        if (action.data.results && action.data.results.length) {
                            this.formState.similarityShow = true;
                            this.rows = action.data.results;
                        } else {
                            this.formState.similarityShow = false;
                        }
                        break;
                    case 'similarity_load':
                            this.formState.similarityLoading = action.data;
                        break;
                }
            })
        }

        this.allowAddRow = this.to.row_detail && this.to.row_detail.allowAddRow;
        // console.log('FormlyDatatablePanelComponent', this);


/*        if (this.field.fieldArray) {
            this.field.fieldArray.fieldGroup.forEach( group => {
                this.opts.push(group.templateOptions);
            });
        }*/



        // console.log('FormlyRepeatSectionComponent ngOnInit', this);

        // console.log('datatable panel',  this.formControl, this.form);
    }

    ngAfterViewInit() {
        this.onResize(true);
    }

/*    ngOnChanges(changes: SimpleChanges) {
        const model: SimpleChange = changes.model;
        console.log('prev value: ', model.previousValue);
        console.log('got name: ', model.currentValue);
        this.rows = model.currentValue;
    }*/

    private onFormValueChange(data) {
        console.log('internal onFormValueChange', this.formControl.status);

        if ( this.formControl.status === 'INVALID') {
            // turn off validation for the master form array
            Object.keys(this.formControl.controls).forEach(key => {
                const field = this.formControl.get(key);
                field.setValidators(null);
                field.updateValueAndValidity();
            });
            // this.model.splice(0, this.model.length);
        }

    }

    ngOnDestroy() {
        this.alive = false;
    }

    private loadData(datasource = this.datasource || this.apipath) {

        if (datasource && datasource.indexOf('page_size')) {
            delete this.filterQuery.page_size;
        }

        this.apiService.getByParams(datasource, this.filterQuery)
            .pipe(takeWhile(() => this.alive))
            .subscribe(
                (res: any) => {
                    // console.log('FormlyDatatablePanelComponent->loadData->response', res);
                    this.rows = res.results;
                    this.rows_count = res.count;
                    this.isLoading = false;
                },
                (err) => {
                    console.log('Error occured', err);
                }
            );
    }

    public setPage(pageInfo) {

        console.log('setPage', pageInfo);

        // set page number on the filter query object
        this.filterQuery.page = String(pageInfo.offset + 1);
        if (this.externalPaging && this.datasource) {
            this.loadData(this.datasource);
        }
    }

    public addRow() {
        // console.log(this);

        const newRow = {};
        let FG = null;

        if (this.field && this.field.fieldArray && Array.isArray(this.field.fieldArray.fieldGroup)) {
            // FIELDGROUP ARRAY
            FG = this.field.fieldArray.fieldGroup;

        } else if (this.field && Array.isArray(this.field.fieldGroup)) {
            // SINGLE FIELDGROUP
            FG = this.field.fieldGroup;

        } else {
            console.log('invalid row_detail in component configuration');
            return false;
        }

        FG.forEach(field => {
            if (typeof field.key === 'string' && field.key !== '') {
                newRow[field.key] = null;
            }
        });

        this.expandRow = this.model.length;

        // force refresh workaround disabled as https://github.com/formly-js/ngx-formly/issues/477 is solved
        // this.forceRefresh();
        if (this.datasource) {
            // console.log(this.datasource);
            this.rows = [...this.rows, newRow];
        } else {
            this.add(null, newRow);
            this.subForm.markAsDirty();
            this.rows = [...this.model];
        }

        // this.columns = [...this.columns];

    }

    public removeRow(id: number, apipath: string = null) {

        // console.log('removeRow', id, apipath);
        if (!apipath || apipath === null) {
            // LOCAL DATA
            this.remove(id);
            this.rows = [...this.model];
            this.form.markAsDirty();
            this.toastr.warning('É necessario salvare la scheda per registrare la modifica', 'Rimozione OK');
        } else {
            // REMOTE DATA
            this.apiService.delete(apipath, id)
                .pipe(takeWhile(() => this.alive))
                .subscribe(
                    (response: any) => {
                        this.toastr.success(null, 'Rimozione OK');
                        this.loadData();
                    }
                );
        }
    }

    private forceRefresh() {
        this.isLoading = true;
        setTimeout(() => {this.isLoading = false}, 1);
    }

    public onActionDone($event: any) {

        // console.log('onActionDone', $event);

        switch ($event.do) {
            case 'delete':
                this.removeRow($event.doParams,  $event.apipath);
                break;
            case 'reload':
                this.loadData();
                break;
            default:

        }
    }

    public onActionError(err: any) {
        // this.toastr.error(err.message, 'Errore ' + err.status, {timeout: 10000});
        console.log('Error occured', err);
    }
}
