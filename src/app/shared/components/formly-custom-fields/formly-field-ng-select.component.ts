import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import {Observable, Subject, of, concat} from 'rxjs';
import {startWith, debounceTime, distinctUntilChanged, switchMap, catchError, tap, map, takeWhile, takeUntil} from 'rxjs/operators';

import {FormlyFieldConfig, FieldType} from '@ngx-formly/core';
import {APICommonService} from '../../../_services/api-common.service';

import get from 'lodash-es/get';

@Component({
    selector: 'app-formly-field-ng-select',
    template: `
        <ng-select
                class="form-control p-0"
                [formControl]="formControl"
                appendTo="body"
                [virtualScroll]="true"
                [items]="items"
                [bindLabel]="labelProp"
                [bindValue]="valueProp"
                [loading]="fieldLoading"
                [typeahead]="input$"
                [formlyAttributes]="field"
                [hideSelected]="true"
                (open)="onOpen()"
                (change)="onChange()"
                (clear)="onClear()"
                (scroll)="onScroll($event)"
                (scrollToEnd)="onScrollToEnd($event)"
                (search)="onSearch($event)"
        >
            <ng-template ng-option-tmp let-item="item" let-search="searchTerm">
                <div [ngOptionHighlight]="search"
                     [innerHTML]="item[labelProp] || item" [title]="item[labelProp] || item"></div>
            </ng-template>
            <ng-template ng-footer-tmp *ngIf="loadedResults && totalResults">
                <small class="form-text">Caricati {{loadedResults}} di {{totalResults}}</small>
            </ng-template>
        </ng-select>
    `,
})
export class FormlyFieldNgSelectComponent extends FieldType implements OnInit, OnDestroy, AfterViewInit {

    items$: Observable<any[]>;
    items:  Array<any> = null;
    input$: Subject<string> = new Subject<string>();

    private alive = true;
    private unsubscribe: Subject<void> = new Subject();

    private term = '';
    private numberOfItemsFromEndBeforeFetchingMore = 10;

    public fieldLoading = false;
    public initLab: string = null;
    public loadedResults = 0;
    public totalResults = 0;

    private initialized = false; // TODO: why double init?
    private _field: FormlyFieldConfig;

    public page = 1;
    private lastFilterVal: any = {};

    get labelProp(): string { return this.to.codeListSimple ? null : this.to.labelProp || 'name'; }
    get valueProp(): string { return this.to.codeListSimple ? null : this.to.valueProp || 'id'; }
    get groupByProp(): string { return this.to.groupByProp || null; }
    get codeListParams(): any { return this.to.codeListParams || {}; }

    // @ts-ignore
    set field(field) {
        // console.log('initialized', field.key, this.initialized);
        this._field = field;
        if (!this.initialized && field.model && field.key) {
            const item = get(this.field.model, field.key);
            // console.log(field.key, 'init with value:', item);
            this.initAutocompleter(item);
            this.initialized = true;
        }
    }
    get field() { return this._field; }

    constructor(
        private apiService: APICommonService,
    ) {
        super();
    }

    public ngOnInit() {
        // console.log('this.formControl', this.model, this.formControl );
        // console.log('form', {...this.form});
        // console.log('formState', {...this.formState});
        // console.log('FormlyFieldNgSelectComponent onInit', this);
    }

    public ngAfterViewInit() {
        // CASCADING SELECTS

        // if the templateOptions sets a filterField parameter
        if (this.to.filterField) {

            if ('string' === typeof this.to.filterField) {
              // console.log('FormlyFieldNgSelectComponent->ngAfterViewInit', this.form);
              this.initFilterField(this.to.filterField);

            } else if (Array.isArray(this.to.filterField)) {
              this.to.filterField.forEach(filterField => {
                this.initFilterField(filterField);
              });
            }

        }
    }

    public ngOnDestroy() {
        // console.log('select destroy');
        this.alive = false;
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    private handleSearchParams (term: string) {
        // console.log('new term: ' + term, 'prev term: ' +  this.term);
        if (this.term !== term) {
            this.term = term;
            // console.log('null value');
            // console.trace('null value');
            this.items = null;
            this.page = 1;
        }

        const SP = this.codeListParams;
        SP.search = this.term;
        if (this.to.codeListPaged) {
            SP.page = this.page;
        }
        if ('string' === typeof this.to.filterField) {
          if (this.lastFilterVal[this.to.filterField] !== -1) {
            SP[this.to.filterField] = this.lastFilterVal[this.to.filterField];
          }
        }
        return SP;
    }

    private initFilterField(filterFieldName) {

      // retrive the field by key (name) on the same form
      const filterField = this.form.get(filterFieldName);
      // stores the filter field current value
      this.lastFilterVal[filterFieldName] = filterField.value;

        // retrive the field by key (name) on the same form
        // console.log('filterField: ', filterField);
        // console.log('filterField.value', filterField.value);

        // subscribe to field value changes
        filterField.valueChanges.pipe(
            takeUntil(this.unsubscribe),
            // takeWhile(() => this.alive),
            startWith(filterField.value), // starting with it's current value
            distinctUntilChanged(),
            tap(value => {
                // when it changes...
                const valueType = typeof value;

                if (valueType === 'string' || valueType === 'number' && value !== this.lastFilterVal[filterFieldName]) {
                    // console.log('valueChange: ' + this.lastFilterVal[filterFieldName], '|', value);
                    // this.formControl.setValue(''); // reset this field value

                    this.items = [];                    // reset the item list
                    this.page = 1;                      // reset the page counter

                    this.loadedResults = 0;          // reset loaded/total results
                    this.totalResults = 0;

                    this.to.codeListParams[filterFieldName] = value; // set codeListParams with the filter name and it's value

                    this.lastFilterVal[filterFieldName] = value;
                }

                // console.log('ngx-select', this);
            }),
            takeUntil(this.unsubscribe)
        ).subscribe();

        // this.formControl.setValue('');
        this.input$.next();
    }

    private parseDefaultValue(defaultValue: string) {

        if (defaultValue.match(/^\${.*}$/g)) {
            // this is a templated query, let's try to retrive a value from the local context removing the ${ } template markers

            // console.log('defaultvalue', defaultValue.substring(2, defaultValue.length - 1));
            defaultValue = get(this, defaultValue.substring(2, defaultValue.length - 1));
        }

        return String(defaultValue);
    }

    private initAutocompleter(item: any) {

        // console.log('initAutocompleter item', item);

        if (!item) {
            if (this.to.defaultValue) {
                item = this.parseDefaultValue(this.to.defaultValue);
            }
        } else {
            // console.log(item[this.valueProp]);
            this.formControl.setValue(item[this.valueProp] || item, {emitEvent: true}); // set initial Value
            this.initLab = item[this.labelProp] || item; // set initial label, used to search onOpen
        }

        //
        // received only an ID ? try to fill the value
        //
        const item2num = Number(item);

        // console.log(this.to.codelistPath, this.field.key, 'item2num', item2num);

        if (this.to.codelistPath && !isNaN(item2num) && item2num !== 0) {
            this.apiService.getById(this.to.codelistPath, item2num)
                .pipe(takeWhile(() => this.alive))
                .subscribe(
                    response => {
                        // console.log(response, this.valueProp, this.labelProp, item2num === response[this.valueProp], response[this.labelProp]);
                        if (item2num === response[this.valueProp] && response[this.labelProp]) {
                            // console.log(this.valueProp, response[this.valueProp], this.labelProp, response[this.labelProp]);
                            this.items = [response];
                            // this.input$.next(response[this.labelProp]);
                            this.formControl.setValue(item2num, {emitEvent: true});
                        }
                        // console.log(response);
                    }, error => {
                        console.log('ng-select received only an ID, tried to fill the value....', error);
                    });
        }

        const defaultItem = item ? [item] : [];

        // console.log(this.field.key, 'defaultItem', defaultItem);

        this.items$ = concat(
            of(defaultItem), // default items
            this.input$.pipe(
                takeWhile(() => this.alive),
                debounceTime(400),
                // distinctUntilChanged(), // can't enable it as scrollToEnd may emit same term with next page
                tap(() => { this.fieldLoading = true }),
                switchMap(term => this.apiService.getByParams(this.to.codelistPath, this.handleSearchParams(term))
                    .pipe(
                        takeWhile(() => this.alive),
                        map( response => { return this.handleResponse(response) }),
                        catchError(() => of([])), // empty list on error
                        tap(() => { this.fieldLoading = false; })
                    )
                )
            )
        );

        this.items$.subscribe(value => {

            // console.log('subscribed loaded', value);
            if (this.page > 1) {
                this.items = this.items.concat(value);
            } else {
                this.items = value;
            }
            this.loadedResults = this.items.length;
        });
    }

    private handleResponse(response): Array<any> {
        // console.log('handleResponse', response);
        this.totalResults = response.count || 0;
        return response.results ? response.results : response;
    }

    public onOpen() {
        // console.log('onOpen');
        // console.log(this.field.model[this.field.key]);
/*        if (!this.to.codeListPaged || !this.loadedResults) {
            this.input$.next(this.term);
        }*/

        this.input$.next(this.term);
    }

    public onChange() {
        if (this.to.onChange) {
            // console.log('FormlyFieldNgSelectComponent onChange', this.form.get('classification'), this);
            // console.log('FormlyFieldNgSelectComponent onChange', this.formControl, {...this.model});
        }
    }

    public onClear() {
        this.formControl.setValue('');
    }

    public onScroll($event) {
        if (this.fieldLoading) {
            return;
        }
        // console.log('onScroll');

        if (this.to.codeListPaged && this.items.length < this.totalResults &&
            $event.end + this.numberOfItemsFromEndBeforeFetchingMore >= this.items.length) {
            this.fieldLoading = true;
            this.page++;
            this.input$.next(this.term);
            // console.log('onScrollLoadNext', $event, this.items.length, this.page);
        }
    }

    public onScrollToEnd($event) {

        if (this.fieldLoading) {
            return;
        }

        // console.log('onScrollToEnd', this.to.codeListPaged, this.items.length, this.totalResults);
        if (this.to.codeListPaged && this.items.length < this.totalResults) {
            this.fieldLoading = true;
            this.page++;
            this.input$.next(this.term);
            // console.log('onScrollToEndLoadNext', $event, this.items.length, this.page);
        }
    }

    public onSearch($event: { term: string; items: any[] }) {
      // console.log('onSearch', $event.term);
    }
}
