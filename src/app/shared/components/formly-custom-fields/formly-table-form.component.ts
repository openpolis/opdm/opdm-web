import { Component, ViewChild, TemplateRef, OnInit } from '@angular/core';
import { FormlyFieldConfig, FieldArrayType, FormlyFormBuilder } from '@ngx-formly/core';
import { TableColumn } from '@swimlane/ngx-datatable';

@Component({
    selector: 'app-formly-field-datatable',
    templateUrl: './formly-table-form.component.html'
})

export class FormlyTableFormComponent extends FieldArrayType implements OnInit {
    @ViewChild('defaultColumn', { static: true }) public defaultColumn: TemplateRef<any>;

    ngOnInit() {
        this.to.columns.forEach(column => column.cellTemplate = this.defaultColumn);
    }

    constructor(builder: FormlyFormBuilder) {
        super(builder);
    }

    getField(field: FormlyFieldConfig, column: TableColumn, rowIndex: number ): FormlyFieldConfig {
        return field.fieldGroup[rowIndex].fieldGroup.find(f => f.key === column.prop);
    }

    getModel(model, column: TableColumn, rowIndex): any {
        return model[rowIndex];
    }
}
