import {Component} from '@angular/core';
import {FieldType} from '@ngx-formly/core';

@Component({
    selector: 'app-formly-field-spacer',
    template: ``,
})
export class FormlyFieldSpacerComponent extends FieldType {
}
