import {Component} from '@angular/core';
import {FieldType, FormlyFieldConfig} from '@ngx-formly/core';

@Component({
    selector: 'app-formly-field-image',
    template: `
        <img [src]="_imgSrc" id="picture" class="form-control" style="max-width: 180px" (error)="updateUrl($event)">
        <input type="hidden" [formControl]="formControl" [formlyAttributes]="field">
    `,
})
export class FormlyFieldImageComponent extends FieldType {

    private _field: FormlyFieldConfig;
    public _imgSrc = '';

    // @ts-ignore
    set field(field) {
        this._field = field;
        if (field.model && field.key) {
            this._imgSrc = this.field.model[<string>this.field.key];
        }
    }
    get field() { return this._field; }

    public updateUrl($event) {
        console.error('immagine rotta', $event);
    }
}
