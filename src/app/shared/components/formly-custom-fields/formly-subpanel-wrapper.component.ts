import { Component, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import {CommonModule} from '@angular/common';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
    selector: 'app-formly-wrapper-subpanel',
    template: `
    <div class="clearfix">
        <div [collapse]="collapsed"><ng-container #fieldComponent></ng-container></div>
        <button type="button" class="btn btn-light btn-block py-0 position-absolute" style="left: 0; right: 0;"
                (click)="toggleCollapse()"
                [attr.aria-expanded]="!collapsed" aria-controls="collapseBasic">
            <span [innerHTML]="collapsed ? '+' : '-'"></span> {{ to.label }}
        </button>
    </div>
  `,
})
export class FormlySubPanelWrapperComponent extends FieldWrapper implements OnInit{
    @ViewChild('fieldComponent', { read: ViewContainerRef, static: true }) fieldComponent: ViewContainerRef;

    public collapsed = true;

    ngOnInit() {
        // console.log('subpanel ' + this.to.label, this.to.closed);
        // console.log('FormlyPanelWrapperComponent', this);
    }

    public toggleCollapse() {
        this.collapsed = !this.collapsed;
    }
}
