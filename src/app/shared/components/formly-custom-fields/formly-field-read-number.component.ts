import { Component } from '@angular/core';
import {FieldType, FormlyFieldConfig} from '@ngx-formly/core';
import get from 'lodash-es/get';

@Component({
    selector: 'app-formly-field-read-number',
    template: `<div class="pl-1 mb-4">
        <span class="h4" title="{{number_alt}}"><i class="{{icon}}"></i
        >{{before_title}}<span *ngIf="number !== ''; else nd;">{{number | number}}</span>{{after_title}}</span>
        <ng-template #nd>n.d.</ng-template>
    </div>`,
})
export class FormlyFieldReadNumberComponent extends FieldType {

    public before_title = '';
    public after_title = '';
    public label = '';
    public number = '';
    public number_alt = '';
    public icon = '';
    public style_class = 'h4';

    private _field: FormlyFieldConfig;

  // @ts-ignore
    set field(field) {
        this._field = field;

        this.processLinkOptions();
    }
    get field(): FormlyFieldConfig { return this._field; }

    private processLinkOptions() {

        if (this.to.number) {
            const T = [];
            this.to.number.forEach(key => {
                const val = get(this.model, key, null);
                T.push(val);
            });
            this.number = T.join(' ');
        }


        ['icon', 'number_alt', 'before_title', 'after_title', 'style_class'].forEach( arg => {
           if (this.to[arg]) {
               this[arg] = this.to[arg];
           }
        });
    }
}
