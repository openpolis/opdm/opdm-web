import {Component, OnInit} from '@angular/core';
import {FieldType, FormlyFieldConfig} from '@ngx-formly/core';

import get from 'lodash-es/get';

@Component({
    selector: 'app-formly-panel-header',
    template: `
        <div class="clearfix mb-4">
            <img *ngIf="_imgSrc" [src]="_imgSrc" [alt]="_title" [title]="_title"
                 class="float-left mr-4" style="height: 75px">
            <h1 class="float-left mr-4" style="font-size: 48px;">{{_section}}</h1>
            <h2 class="pb-0 mb-0">{{_title}}</h2>
            <h5>{{_subtitle}}</h5>
        </div>
    `,
})
export class FormlyPanelHeaderComponent extends FieldType implements OnInit {

    public _imgSrc: string;
    public _title: string;
    public _subtitle: string;
    public _section: string;

    private _field: FormlyFieldConfig;

  // @ts-ignore
/*    set field(field) {
        const TO = field.templateOptions;
        if (field.model && TO && TO.headerOptions) {
            this.processHeaderOptions(field.model, TO.headerOptions);
        }
    }
    get field() { return this._field; }*/

    public ngOnInit() {
      const TO = this.field.templateOptions;
      if (this.field.model && TO && TO.headerOptions) {
        this.processHeaderOptions(this.field.model, TO.headerOptions);
      }
    }
    public processHeaderOptions (model, o: HeaderOptions) {
        if (o.title) {
            const title = [];
            o.title.forEach(key => {
                title.push(get(model, key));
            });
            this._title = title.join(' ');
        }

        if (o.subtitle) {
            const subtitle = [];
            o.subtitle.forEach(key => {
                subtitle.push(get(model, key));
            });
            this._subtitle = subtitle.join(' - ');
        }

        if (o.image) {
            this._imgSrc = model[o.image];
        }

        if (o.section) {
           this._section = o.section;
        }
    }
}

interface HeaderOptions {
    title: string[];
    subtitle: string[];
    image: string;
    section: string;
}
