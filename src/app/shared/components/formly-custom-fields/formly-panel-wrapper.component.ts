import { Component, ViewChild, ViewContainerRef, OnInit } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
    selector: 'app-formly-wrapper-panel',
    template: `
    <div class="clearfix"></div>
    <accordion [style.display]="to.hidden ? 'none' : 'inherit'">
        <accordion-group #accordionPanel [isOpen]='!to.closed' [formlyAttributes]="field">
            <div accordion-heading class="clickable clearfix">
                <strong>{{ to.label }}</strong>
                <span class="float-right pull-right accordion-toggle-icon"
                      [class.accordion-open-icon]="accordionPanel.isOpen"
                      [class.accordion-close-icon]="!accordionPanel.isOpen">
                        <span class="fas fa-chevron-down"></span>
                        <span class="fas fa-chevron-right"></span>
                </span>
            </div>
            <ng-container #fieldComponent></ng-container>
        </accordion-group>
    </accordion>
  `,
})
export class FormlyPanelWrapperComponent extends FieldWrapper implements OnInit{
    @ViewChild('fieldComponent', { read: ViewContainerRef, static: true }) fieldComponent: ViewContainerRef;

    ngOnInit() {
        // console.log('panel ' + this.to.label, this);
        // console.log('FormlyPanelWrapperComponent', this);

    }
}
