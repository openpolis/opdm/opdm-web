import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { FieldArrayType, FormlyFormBuilder, FormlyFormOptions, FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import cloneDeep from 'lodash-es/cloneDeep';
import {DatatableComponent} from '@swimlane/ngx-datatable';

@Component({
    selector: 'app-formly-repeat2-section',
    templateUrl: 'formly-datatable2-panel.component.html'
})
export class FormlyDatatable2PanelComponent extends FieldArrayType implements OnInit {
    @ViewChild(DatatableComponent, { static: true }) table: DatatableComponent;

    /* TABLE HEADERS DEFINITION BINDINGS */
    @ViewChild('T_hdr', { static: true }) T_hdr: TemplateRef<any>;
    @ViewChild('T_hdr_sel', { static: true }) T_hdr_sel: TemplateRef<any>;
    @ViewChild('T_hdr_exp', { static: true }) T_hdr_exp: TemplateRef<any>;

    /* TABLE CELLS DEFINITION BINDINGS */
    @ViewChild('T_exp', { static: true }) T_exp: TemplateRef<any>;
    @ViewChild('T_exp_badge', { static: true }) T_exp_badge: TemplateRef<any>;
    @ViewChild('T_id', { static: true }) T_id: TemplateRef<any>;
    @ViewChild('T_ddMMyyyy', { static: true }) T_ddMMyyyy: TemplateRef<any>;
    @ViewChild('T_ddMMyyyyHHmmss', { static: true }) T_ddMMyyyyHHmmss: TemplateRef<any>;
    @ViewChild('T_identifiers', { static: true }) T_identifiers: TemplateRef<any>;
    @ViewChild('T_gender', { static: true }) T_gender: TemplateRef<any>;
    @ViewChild('T_contact_details', { static: true }) T_contact_details: TemplateRef<any>;
    @ViewChild('T_personlink', { static: true }) T_personlink: TemplateRef<any>;
    @ViewChild('T_person', { static: true }) T_person: TemplateRef<any>;
    @ViewChild('T_memberperson', { static: true }) T_memberperson: TemplateRef<any>;
    @ViewChild('T_organization', { static: true }) T_organization: TemplateRef<any>;
    @ViewChild('T_membership', { static: true }) T_membership: TemplateRef<any>;
    @ViewChild('T_memberships', { static: true }) T_memberships: TemplateRef<any>;
    @ViewChild('T_simil_add', { static: true }) T_simil_add: TemplateRef<any>;
    @ViewChild('T_simil_bind', { static: true }) T_simil_bind: TemplateRef<any>;
    @ViewChild('T_sep_num', { static: true }) T_sep_num: TemplateRef<any>;
    @ViewChild('T_text', { static: true }) T_text: TemplateRef<any>;
    @ViewChild('T_org_members', { static: true }) T_org_members: TemplateRef<any>;
    @ViewChild('T_empty', { static: true }) T_empty: TemplateRef<any>;
    @ViewChild('T_delete', { static: true }) T_delete: TemplateRef<any>;

    // empty Templates holders (defined OnInit as @ViewChild references are not available before)
    private headerTemplates: {} = null;
    private cellTemplates: {} = null;

    public columns = [];
    public rows = [];

    public allowAddRow = false;

    public detailRowModel: any;
    public rowsDetailOpen: number = null;

    showAddForm = false;
    showEditForm = false;

    singleForm = new FormGroup({});
    singleField = [];
    singleOptions: any = {};
    singleModel = {};
    index = -1;

    constructor(builder: FormlyFormBuilder) {
        super(builder);
    }

    ngOnInit() {
        this.initTemplates();
        this.singleField = cloneDeep(this.field.fieldArray.fieldGroup);

        this.columns = this.prepareColumns(this.field.templateOptions.columns);
        this.rows = [...this.model];

        console.log('formControl:', this.formControl);

        this.allowAddRow = this.to.row_detail && this.to.row_detail.allowAddRow;

    }

    //
    // NGX-DATATABLE HEADER AND CELLS
    //
    private initTemplates() {
        // console.log('INIT TEMPLATES');
        this.headerTemplates = {
            hdr: this.T_hdr,
            hdr_sel: this.T_hdr_sel,
            hdr_exp: this.T_hdr_exp
        };

        this.cellTemplates = {
            exp: this.T_exp,
            exp_badge: this.T_exp_badge,
            id: this.T_id,
            ddMMyyyy: this.T_ddMMyyyy,
            ddMMyyyyHHmmss: this.T_ddMMyyyyHHmmss,
            identifiers: this.T_identifiers,
            gender: this.T_gender,
            contact_details: this.T_contact_details,
            personlink: this.T_personlink,
            person: this.T_person,
            memberperson: this.T_memberperson,
            organization: this.T_organization,
            membership: this.T_membership,
            memberships: this.T_memberships,
            simil_add: this.T_simil_add,
            simil_bind: this.T_simil_bind,
            sep_num: this.T_sep_num,
            text: this.T_text,
            org_members: this.T_org_members,
            empty: this.T_empty,
            delete: this.T_delete
        };
    }

    private prepareColumns(columns: Array<any>): Array<any> {
        console.log('PREPARE COLUMNS', columns);
        columns.forEach(col => this.parseOptions(col));

        // console.log(columns);
        return columns;
    }

    private parseOptions(col) {

        if (typeof col.head === 'string') {
            col.headerTemplate = this.headerTemplates[col.head];
        }

        if (typeof col.cell === 'string') {
            col.cellTemplate = this.cellTemplates[col.cell];
        }

        col.resizeable = col.resize || 0;
        col.sortable = col.sort || 0;
        col.draggable = col.drag || 0;
    }

    addItem(value) {
        this.showAddForm = true;
    }

    editItem(index) {
        this.index = index;
        this.singleModel = cloneDeep(this.model[index]);
        this.showEditForm = true;
    }

    hideAddForm() {
        this.showAddForm = false;
        this.singleOptions.resetModel();
        this.index = -1;
    }

    save(model) {
        if (this.index === -1) {
            this.add(null, model);
        } else {
            this.formControl.at(this.index).patchValue(model);
        }
        this.hideAddForm();
    }

    /* TABLE ROW DETAIL TOGGLE */
    public toggleExpandRow(row) {

        const rowIndex = this.rows.findIndex( (item) => {

            // if (item === row) { console.log('same obj found'); }
            return item === row;
            // return item.id === row.id;
        });

        this.editItem(rowIndex);

        if (this.rowsDetailOpen === null) {
            // nothing is already opened
            // console.log('// nothing is already opened');
            this.rowsDetailOpen = rowIndex;
            this.table.rowDetail.toggleExpandRow(row);
        } else if (this.rowsDetailOpen === rowIndex) {
            // the same is already opened
            // console.log('// the same is already opened');
            this.rowsDetailOpen = null;
            this.table.rowDetail.toggleExpandRow(row);
        } else {
            // another is already opened
            // console.log('// another is already opened');
            this.table.rowDetail.collapseAllRows();
            /*            setTimeout(() => {
                            this.table.rowDetail.toggleExpandRow(row);
                            this.rowsDetailOpen = rowIndex;
                        }, 10);*/

            this.table.rowDetail.toggleExpandRow(row);
            this.rowsDetailOpen = rowIndex;
        }

        // this.cdRef.markForCheck();

        return false;
    }

    public onDetailToggle(event) {
        // console.log('DatatablePanelComponent->onDetailToggle', event);
        // this.detailRowModel = event.value;
    }
}
