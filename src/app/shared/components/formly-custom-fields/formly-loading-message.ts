import {Component} from '@angular/core';
import {FieldType, FormlyFieldConfig} from '@ngx-formly/core';

@Component({
    selector: 'app-formly-loading-message',
    template: `<div class="text-center">{{to.label}} <i class="fas fa-circle-notch fa-spin text-muted"></i></div>`,
})
export class FormlyLoadingMessageComponent extends FieldType {

}
