import { Component } from '@angular/core';
import {FieldType, FormlyFieldConfig} from '@ngx-formly/core';
import get from 'lodash-es/get';

@Component({
    selector: 'app-formly-field-link',
    template: `<div class="pl-1 mb-4">
        <span class="h4"><i class="{{icon}}"></i
        >{{before_title}}<a [routerLink]="[apipath + link]"
                             [ngClass]="style_class"
                             [queryParams]="query"
                             title="{{title_alt}}">{{title}}</a>{{after_title}}</span>
    </div>`,
})
export class FormlyFieldReadLinkComponent extends FieldType {

    public before_title = '';
    public after_title = '';
    public label = '';
    public title = '';
    public title_alt = '';
    public link = '';
    public apipath = '';
    public icon = '';
    public query = {};
    public style_class = 'h4';

    private _field: FormlyFieldConfig;

  // @ts-ignore
    set field(field) {
        this._field = field;

        this.processLinkOptions();
    }
    get field(): FormlyFieldConfig { return this._field; }

    private processLinkOptions() {
        if (this.to.title) {
            const T = [];
            this.to.title.forEach(key => {
                T.push(get(this.model, key, key));
            });
            this.title = T.join(' ');
        }

        if (this.to.link) {
            this.link = this.mapParamToModel(this.to.link, '/');
        }

        ['apipath', 'query', 'icon', 'title_alt', 'before_title', 'after_title', 'style_class'].forEach( arg => {
           if (this.to[arg]) {
               this[arg] = this.to[arg];
           }
        });

        if (this.to.query) {
            this.query = {};
            Object.keys(this.to.query).map(key => {
                const V = this.to.query[key];

                if (Array.isArray(V)) {
                    this.query[key] = this.mapParamToModel(V);
                } else {
                    this.query[key] = V;
                }
            });
        }
    }

    private mapParamToModel(params: string[], separator: string = ''): string {
        const ret = [];
        params.forEach(entry => {
            ret.push(get(this.model, entry))
        });
        return ret.join(separator);
    }
}
