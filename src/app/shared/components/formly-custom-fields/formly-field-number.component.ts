import {Component} from '@angular/core';
import {FieldType} from '@ngx-formly/core';

// APP CONFIG
import {AppConfig} from '../../../app.config';

@Component({
    selector: 'app-formly-field-number',
    template: `
            <input type="text" [formControl]="formControl" class="form-control" [formlyAttributes]="field" [textMask]="config.numberMask">
    `,
})
export class FormlyFieldNumberComponent extends FieldType {
    constructor (
        public config: AppConfig
    ) {
        super();
    }
}
