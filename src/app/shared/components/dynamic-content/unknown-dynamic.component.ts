import {Component} from '@angular/core';

@Component({
    selector: 'app-unknown-component',
    template: `<div>Unknown component ({{model | json}})</div>`
})
export class UnknownDynamicComponent {
    model: any;
    constructor() {
        console.log('UnknownDynamicComponent please check your config', this.model);
    }
}