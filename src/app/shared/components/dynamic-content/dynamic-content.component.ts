import {Component, ComponentFactoryResolver, ComponentRef, Input, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {FormPanelComponent, TablePanelComponent} from '..';
import {UnknownDynamicComponent} from './unknown-dynamic.component';

@Component({
    selector: 'app-dynamic-content',
    template: `
        <div>
            <div #container></div>
        </div>
    `
})
export class DynamicContentComponent implements OnInit, OnDestroy {
    @ViewChild('container', { read: ViewContainerRef, static: true })
    container: ViewContainerRef;

    @Input() model: any;
    @Input() options: any;
    @Input() form: any;
    @Input() fields: any;

    private mappings = {
        'table': TablePanelComponent,
        'form': FormPanelComponent
    };

    private componentRef: ComponentRef<{}>;

    constructor(
        private componentFactoryResolver: ComponentFactoryResolver) {
    }

    getComponentType(typeName: string) {
        const type = this.mappings[typeName];
        return type || UnknownDynamicComponent;
    }

    ngOnInit() {
        console.log(this);
        if (this.options.type) {
            const componentType = this.getComponentType(this.options.type);

            // note: componentType must be declared within module.entryComponents
            const factory = this.componentFactoryResolver.resolveComponentFactory(componentType);
            this.componentRef = this.container.createComponent(factory);

            // set component context
            const instance = <any> this.componentRef.instance;

            // copy configuration options to object instance
            if (this.options) {
                for (const k of Object.keys(this.options)) {
                    instance[k] = this.options[k];
                }
            } else {
                // console.log('dynamic-content.component: missing options')
            }

            // console.log('form', this.form);
            instance.model = this.model;
            instance.form = this.form;

            console.log('fields', this.fields);

            if (this.fields && this.fields.fieldArray && this.fields.fieldArray.fieldGroup) {
                instance.fields = this.fields.fieldArray.fieldGroup;
            } else {
                instance.fields = this.fields;
            }
        }
    }

    ngOnDestroy() {
        if (this.componentRef) {
            this.componentRef.destroy();
            this.componentRef = null;
        }
    }

}


