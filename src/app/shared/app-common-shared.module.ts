// ANGULAR MODULES
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule, FormControl, ValidationErrors} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {CommonModule} from '@angular/common';

// VENDOR MODULES
import {LaddaModule} from 'angular2-ladda';
import {TextMaskModule} from 'angular2-text-mask';
import {NgSelectModule} from '@ng-select/ng-select';
import {NgOptionHighlightModule} from '@ng-select/ng-option-highlight';


import {AccordionModule} from 'ngx-bootstrap/accordion';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ModalModule} from 'ngx-bootstrap/modal';

import {NgxDatatableModule} from '@swimlane/ngx-datatable';

import {FormlyModule} from '@ngx-formly/core';
import {FormlyBootstrapModule} from '@ngx-formly/bootstrap';
import {NgxJsonViewerModule} from 'ngx-json-viewer';

// APP COMPONENTS
import {
    FormlyPanelWrapperComponent, FormlySubPanelWrapperComponent,
    FormlyFieldNgSelectComponent, FormlyFieldImageComponent,
    FormlyFieldNumberComponent, FormlyFieldSpacerComponent, FormlyFieldReadLinkComponent, FormlyFieldReadNumberComponent, FormlyFieldButtonComponent,
    FormlyDatatablePanelComponent, FormlyDatatable2PanelComponent,
    FormlyPanelHeaderComponent, FormlyTableFormComponent, FormlyLoadingMessageComponent
} from './components/formly-custom-fields';
// import {formlyFieldTransformSample} from '../../../shared/components/formly-custom-fields/formly-field.transform.sample';

import {ContactIconComponent, DatatablePanelComponent, DebugPanelComponent, IdentifierIconComponent,
        FiltersPanelComponent, BatchActionsPanelComponent, TablePanelComponent, FormPanelComponent} from './components/';

import {SimilarityAddComponent, SimilarityBindComponent, ActionItemDeleteComponent} from './components/datatable-panel/actions/';


import {UnknownDynamicComponent, DynamicContentComponent} from './components/dynamic-content';
import { PatchSelectComponent } from './components/patch-select/patch-select.component';


// APP VALIDATORS
export function IntegerValidator(control: FormControl) {
    return /^\d+$/.test(control.value);
}

export function GenericDateValidator(control: FormControl): ValidationErrors {
    return !control.value ||
    /^\d{4}((-0[1-9]|-1[012])(-0[1-9]|-[12][0-9]|-3[01])?)?$/
    .test(control.value) ? null : {'date': true};
}

export function EmailValidator(control: FormControl) {
    return !control.value ||
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    .test(control.value) ? null : {'email': true};
}

export function WebUrlValidator(control: FormControl) {

    // https://gist.github.com/dperini/729294
    // https://mathiasbynens.be/demo/url-regex

    /* tslint:disable */
    return !control.value ||
    /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z0-9\u00a1-\uffff][a-z0-9\u00a1-\uffff_-]{0,62})?[a-z0-9\u00a1-\uffff]\.)+(?:[a-z\u00a1-\uffff]{2,}\.?))(?::\d{2,5})?(?:[/?#]\S*)?$/i
    .test(control.value) ? null : {'url': true};
    /* tslint:enable */
}


@NgModule({
    imports: [
        // ANGULAR MODULES
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        // VENDOR MODULES
        TextMaskModule,
        NgSelectModule,
        NgOptionHighlightModule,
        AccordionModule.forRoot(),
        CollapseModule.forRoot(),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        LaddaModule.forRoot({
            style: 'zoom-in'
        }),
        NgxDatatableModule,
        FormlyBootstrapModule,
        FormlyModule.forRoot(
            {
                wrappers: [
                    {name: 'panel', component: FormlyPanelWrapperComponent},
                    {name: 'subpanel', component: FormlySubPanelWrapperComponent},
                ],
                validators: [
                    {name: 'date', validation: GenericDateValidator},
                    {name: 'email', validation: EmailValidator},
                    {name: 'url', validation: WebUrlValidator}
                ],
                validationMessages: [
                    {name: 'date', message: 'Data non valida'},
                    {name: 'email', message: 'Email non valida'},
                    {name: 'url', message: 'URL non valida'},
                ],
                types: [
                    {
                        name: 'date', extends: 'input', wrappers: ['form-field'],
                        defaultOptions: {
                            validators: {validation: ['date']},
                            templateOptions: {placeholder: 'aaaa[-mm][-gg]'}
                        }
                    },
                    {
                        name: 'email', extends: 'input', wrappers: ['form-field'],
                        defaultOptions: {
                            validators: {validation: ['email']},
                            templateOptions: {placeholder: 'nome@dominiomail.com'}
                        }
                    },
                    {
                        name: 'url', extends: 'input', wrappers: ['form-field'],
                        defaultOptions: {
                            validators: {validation: ['url']},
                            templateOptions: {placeholder: 'https://...'}
                        }
                    },
                    {name: 'read_number', component: FormlyFieldReadNumberComponent, wrappers: ['form-field']},
                    {name: 'select2', component: FormlyFieldNgSelectComponent, wrappers: ['form-field']},
                    {name: 'image', component: FormlyFieldImageComponent, wrappers: ['form-field']},
                    {name: 'link', component: FormlyFieldReadLinkComponent, wrappers: ['form-field']},
                    {name: 'number', component: FormlyFieldNumberComponent, wrappers: ['form-field']},
                    {name: 'spacer', component: FormlyFieldSpacerComponent, wrappers: ['form-field']},
                    {
                        name: 'button', component: FormlyFieldButtonComponent, wrappers: ['form-field'],
                        defaultOptions: {
                            templateOptions: {
                                btnType: 'default',
                                type: 'button'
                            }
                        }
                    },
                    {name: 'datatable', component: FormlyDatatablePanelComponent},
                    {name: 'datatable2', component: FormlyDatatable2PanelComponent},
                    {name: 'header', component: FormlyPanelHeaderComponent},
                    {name: 'message', component: FormlyLoadingMessageComponent},
                    {
                        name: 'tableform', component: FormlyTableFormComponent,
                        defaultOptions: {
                            templateOptions: {
                                columnMode: 'force',
                                rowHeight: 'auto',
                                headerHeight: '40',
                                footerHeight: '40',
                                limit: '10',
                                scrollbarH: 'true',
                                reorderable: 'reorderable',
                            },
                        },
                    }
                ],
                /*
                extras: {
                    fieldTransform: [formlyFieldTransformSample],
                }
                */
            }
        ),
        NgxJsonViewerModule
    ],
    declarations: [
        // APP COMPONENTS
        ContactIconComponent,
        DatatablePanelComponent,
        DebugPanelComponent,
        IdentifierIconComponent,
        FiltersPanelComponent,
        BatchActionsPanelComponent,
        TablePanelComponent,
        FormPanelComponent,
        SimilarityAddComponent,
        SimilarityBindComponent,
        ActionItemDeleteComponent,
        UnknownDynamicComponent,
        DynamicContentComponent,
        PatchSelectComponent,
      // FORMLY CUSTOM COMPONENTS
        FormlyPanelWrapperComponent,
        FormlySubPanelWrapperComponent,
        FormlyFieldNgSelectComponent,
        FormlyFieldImageComponent,
        FormlyFieldReadLinkComponent,
        FormlyFieldReadNumberComponent,
        FormlyFieldNumberComponent,
        FormlyFieldSpacerComponent,
        FormlyFieldButtonComponent,
        FormlyDatatablePanelComponent,
        FormlyDatatable2PanelComponent,
        FormlyPanelHeaderComponent,
        FormlyLoadingMessageComponent,
        FormlyTableFormComponent,
        DatatablePanelComponent,
    ],
/*    entryComponents: [
        UnknownDynamicComponent,
        TablePanelComponent,
        FormPanelComponent
    ],*/
    providers: [
    ],
    exports: [
        // ANGULAR MODULES
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        // VENDOR MODULES
        TextMaskModule,
        NgSelectModule,
        NgOptionHighlightModule,
        AccordionModule,
        CollapseModule,
        BsDropdownModule,
        ModalModule,
        LaddaModule,
        NgxDatatableModule,
        // APP COMPONENTS
        ContactIconComponent,
        DatatablePanelComponent,
        DebugPanelComponent,
        IdentifierIconComponent,
        FiltersPanelComponent,
        BatchActionsPanelComponent,
        TablePanelComponent,
        FormlyLoadingMessageComponent,
        FormPanelComponent,
        SimilarityAddComponent,
        SimilarityBindComponent,
        ActionItemDeleteComponent,
        UnknownDynamicComponent,
        DynamicContentComponent,
        // APP VALIDATORS
        // GenericDateValidator
        FormlyModule
    ]
})
export class AppCommonSharedModule {}
