import {forEach} from 'lodash-es';

export class APISerializer {

    private data: any;
    private data_type: string;

    constructor(data_type: string, data: any) {
        this.data_type = data_type;
        this.data = Object.assign({}, data);
    }
    public serialize() {
        const D = {...this.data};

        switch (this.data_type) {
            case 'memberships':
                if (Array.isArray(D.classifications)) {
                    D.classifications = D.classifications.map(item => { return {classification: item.id}; })
                }
                if (!D.post_role && D.post && D.post.role) {
                    D.post_role = D.post.role;
                }
                delete D.post;
                break;
            case 'persons':
                if (D.original_education_level) {
                    D.original_education_level = D.original_education_level.id;
                }
                if (Array.isArray(D.classifications)) {
                  D.classifications = D.classifications.map(item => { return {classification: item.id}; })
                }

                console.log(D);
                break;
            case 'organizations':
                if (Array.isArray(D.key_events)) {
                    D.key_events = D.key_events.map(item => { return {key_event: item.id}; })
                }

                if (Array.isArray(D.classifications)) {
                    D.classifications = D.classifications.map(item => { return {classification: item.id}; })
                }
                break;
            default:
                // console.log({errMsg: 'APISerializer: data_type not supported'});
        }

        delete D.ACTIONBUS;

        this.serializeNested(D);


        return D;
    }

    serializeField(key, D) {
        switch (key) {
            case 'start_date':
            case 'end_date':
            case 'founding_date':
            case 'dissolution_date':
                // set empty start_date, end_date to null
                if (D[key] === '') {
                    D[key] = null;
                }
        }
    }

    serializeNested(D) {
        Object.keys(D).forEach(key => {
            if (Array.isArray(D[key])) {
                D[key].forEach(item => this.serializeNested)
            } else {
                this.serializeField(key, D);
            }
        });
    }
}
