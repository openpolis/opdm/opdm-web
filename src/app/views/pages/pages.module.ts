import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {P404Component} from './404.component';
import {P500Component} from './500.component';
import {LoginComponent} from './login.component';
import {LogoutComponent} from './logout.component';
import {RegisterComponent} from './register.component';

import {PagesRoutingModule} from './pages-routing.module';
import {FormsModule} from '@angular/forms';
import {AuthenticationService} from '../../_services/authentication.service';

@NgModule({
    imports: [
        PagesRoutingModule,
        FormsModule,
        CommonModule
    ],
    declarations: [
        P404Component,
        P500Component,
        LoginComponent,
        LogoutComponent,
        RegisterComponent
    ],
    providers: [
        AuthenticationService
    ]
})
export class PagesModule {
}
