// ANGULAR MODULES
import {NgModule, LOCALE_ID} from '@angular/core';

// VENDOR MODULES


// APP MODULES
import {AppCommonSharedModule} from '../../../shared/app-common-shared.module';

// APP COMPONENTS
import {AbstractListComponent} from './abstract-list.component';

// ROUTING
import {AbstractListRoutingModule} from './abstract-list-routing.module';


@NgModule({
    imports: [
        AppCommonSharedModule,
        AbstractListRoutingModule
    ],
    declarations: [
        AbstractListComponent
    ],
    providers: [
        {provide: LOCALE_ID, useValue: 'it-IT'}
    ]
})
export class AbstractListModule {
}
