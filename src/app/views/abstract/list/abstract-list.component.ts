// ANGULAR MODULES
import {Component, ViewChild, OnDestroy, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {FormGroup} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Title} from '@angular/platform-browser';

// RXJS MODULES
import {takeWhile} from 'rxjs/operators';

// VENDOR MODULES
import {DatatableComponent} from '@swimlane/ngx-datatable';

import intersectionBy from 'lodash-es/intersectionBy';

// APP SERVICES
import {APICommonService} from '../../../_services/api-common.service';
import {DownloadCsvService} from '../../../_services/download-csv.service';


// APP CONFIG
import {AppConfig} from '../../../app.config';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {APISerializer} from '../../../serializers/generic';
import {ToastrService} from 'ngx-toastr';


@Component({
    selector: 'app-abstract-list',
    templateUrl: './abstract-list.component.html'
})
export class AbstractListComponent implements OnDestroy, OnInit {
    @ViewChild(DatatableComponent) table: DatatableComponent;

    private headerTemplates = {};
    private cellTemplates = {};

    private alive = true;

    public title = '';

    public rows = [];
    public rows_count = 0;
    public offset_current = 0;
    public isLoading = true;

    public rowsDetailOpen: number = null;

    public filterQuery: any = {
        page_size: 15,
        page: 1
    };

    public creationQuery: any = {};

    public TPLoptions: any;

    public filters: FormlyFieldConfig[] = [];
    public allFilters = [];
    public filterForm = new FormGroup({});

    public actions: FormlyFieldConfig[] = [];
    public actionForm = new FormGroup({});

    public tableForm = new FormGroup({});

    public columns = [];

    public content_type: string;
    public action: string;
    public apipath: string;

    public static_data: any = {};

    public selected = [];

    private resizeTimeout = null;

    @HostListener('window:resize', ['$event']) onResize(event) {
        // workaround for detail row not resizing on window resize
        // https://github.com/swimlane/ngx-datatable/issues/1355
        clearTimeout(this.resizeTimeout);
        this.resizeTimeout = setTimeout(() => {
            this.columns = [...this.columns];
        }, 80);
    }

    constructor(
        private datePipe: DatePipe,
        private apiService: APICommonService,
        private router: Router,
        private route: ActivatedRoute,
        private titleService: Title,
        private toastr: ToastrService,
        public downloadService: DownloadCsvService,
        public config: AppConfig
    ) {
        // router events subscription
        this.router.events
            .pipe(
                takeWhile(() => this.alive) // unsubscribe when "alive" prop is false
            )
            .subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    this.handleRouteEvent();
                }
            });
    }

    public ngOnInit() {
        // this.handleRouteEvent();
    }

    private handleRouteEvent() {
        // get the current "apipath" route parameter
        this.content_type = this.route.snapshot.params['content_type'];
        this.action = this.route.snapshot.params['action'];

        // merges an eventual action with the content type
        const apipath = this.content_type += this.action ? '/' + this.action : '';

        // console.group();
        // console.log('== router.events ==');
        // console.log('new apipath: ' + apipath);
        // console.log('prev apipath: ' + this.apipath);
        // console.log('action: ', this.action);
        // console.log('queryParams: ', this.route.snapshot.queryParams);
        // console.groupEnd();

        // configure the template when apipath changes or not initialized yet
        if (apipath !== this.apipath) {
            this.filterForm.reset();          // resets the filters formGroup
            this.resetPage();                 // resets the table page
            this.configureTemplate(apipath);
        }

        // this.filterQuery = Object.assign({}, this.filterQuery, this.route.snapshot.queryParams );
        this.filterQuery = Object.assign({}, this.route.snapshot.queryParams );
        this.filterQuery = {...this.filterQuery};
        this.creationQuery = this.getCreationQuery(this.filterQuery);

        this.loadData();                                                    // load the new data
    }

    public ngOnDestroy() {
        this.alive = false;
    }

    private configureTemplate(apipath: string) {

        this.apipath = apipath;

        this.rows = [];
        this.rows_count = 0;

        const TPL = this.config.getListTemplate(this.apipath, this);           // retrieves the abstract configuration template

        this.title = TPL.title;
        this.columns = TPL.columns;
        this.allFilters = TPL.filters;
        this.actions = TPL.actions;

        this.TPLoptions = TPL.options;

        this.filters = [...this.allFilters];                                   // clones the table filters model to current

        this.prefetchData(TPL.prefetch);

        this.route.snapshot.parent.data.title = this.title;                    // set parent router title to apipath label
        this.titleService.setTitle(this.title + ' - ' + this.config.appName);  // set HTML page title
    }

    private prefetchData(prefetch) {
      if (Array.isArray(prefetch) && prefetch.length) {
        prefetch.forEach(item => {
          this.static_data[item.name] = this.apiService.prefetchByParams(item.codelistPath, item.codelistParams || {});
        });
      }

    }
    private getCreationQuery(filterQuery) {

        const Q = {};
        if (this.TPLoptions && this.TPLoptions.supports_creation && this.TPLoptions.creation_params) {

            Object.keys(this.TPLoptions.creation_params).map(e => {

                if (filterQuery[this.TPLoptions.creation_params[e]]) {
                    Q[e] = filterQuery[this.TPLoptions.creation_params[e]];
                }
            });
            // console.log(Q);
        }
        return Q;
    }

    public setFilters(filters) {

        // console.group();
        // console.log('== setFilters ==');
        // console.log('setFilters', filters);

        // loop all existing filter groups
        this.allFilters.forEach( (item) => {

            // for each filters group arrays
            if (Array.isArray(item.fieldGroup)) {

                // process the field data
                item.fieldGroup.forEach(field => {

                    if (field.type === 'number' && filters[field.key]) {
                        // unmask number field (removes separation dots)
                        filters[field.key] = filters[field.key].replace(/\D+/g, '');
                    }
                });
            }
        });

        // delete null filter parameters
        Object.keys(filters).forEach((key) => ( filters[key] === null && delete filters[key] ) );

        // merge form filter parameters with previous filter query object
        const newFilters = Object.assign({}, this.filterQuery, filters);

        // delete empty string filter parameters
        // Object.keys(newFilters).forEach((key) => ( newFilters[key] === '' && delete newFilters[key] ) );

        // update current table offset with page filter or back to offset zero
        this.offset_current = 0;
        newFilters.page = 1;

        // console.log('setFilters newFilters', newFilters);

        // update the routing path with new filters query parameters
        this.router.navigate([], { relativeTo: this.route, queryParams: newFilters });

        // // console.groupEnd();
    }

    public setPage(pageInfo) {

        // set page number on the filter query object
        this.filterQuery.page = String(pageInfo.offset + 1);

        // request data to the API
        this.router.navigate([], { relativeTo: this.route, queryParams: this.filterQuery });
        // this.loadData();
    }

    public onSort(event) {
        // console.log('onSort', event);

        if (event && event.sorts && event.sorts[0]) {
            const direction = event.sorts[0].dir === 'asc' ? '' : '-';
            const prop = event.column.sortBy ||  event.sorts[0].prop;
            this.filterQuery.ordering = direction + prop;

            // request data to the API
            this.router.navigate([], { relativeTo: this.route, queryParams: this.filterQuery });
            // this.loadData();
        }
    }

    public onSelect($event) {
        this.selected = $event;
        // console.log('onSelect', this.selected);
    }

    public onAction($event) {
        const serializer = new APISerializer(this.apipath, $event);
        const submitData = serializer.serialize();

        const selId = this.selected.map( sel => { return {id: sel.id, ...submitData} });
        // console.log(selId);

        this.apiService.patch(this.apipath, selId).subscribe(
            response => {
                this.toastr.success(null, 'Salvataggio OK');
                this.loadData();
                console.log('batch action done:', response);
            },
            error => {
                console.log('batch action failed:', error);
            })
    }

    private loadData() {
        // clone current filters to param object
        const queryParams = {...this.filterQuery};

        // console.log('loadData', queryParams);

        // delete null, undefined & empty string filters keys from param object (unsupported by the API)
        Object.keys(queryParams).forEach((key) => ( !queryParams[key] && delete queryParams[key] ) );

        // convert query params JS dates to API supported string format
        ['active_at', 'active_from', 'active_to', 'updated_after'].forEach( dateparam => {
            if (queryParams[dateparam]) {
                queryParams[dateparam] = this.datePipe.transform(queryParams[dateparam], 'yyyy-MM-dd');
            }
        });

        this.apiService.getByParams(this.apipath, queryParams)
            .pipe(takeWhile(() => this.alive))
            .subscribe(
            (res: any) => {
                // console.log(data);
                this.rows_count = res.count;
                this.rows = res.results;
                this.offset_current = queryParams.page ? queryParams.page - 1 : 0;
                this.isLoading = false;

                // setTimeout(() => { this.restoreDetailOpenStatus()}, 500);
            },
            (err) => {
                this.toastr.error(err.message, 'Errore ' + err.status, {timeOut: 10000});
                console.log('Error occured', err);
            }
        );
    }

    private restoreDetailOpenStatus() {
        this.table.rowDetail.toggleExpandRow(this.rows[this.rowsDetailOpen]);
    }

    public removeRow(id: number, apipath: string = null) {

        // REMOTE DATA
        this.apiService.delete(apipath, id)
            .pipe(takeWhile(() => this.alive))
            .subscribe(
                (response: any) => {
                    this.toastr.success(null, 'Rimozione OK');
                    this.loadData();
                }
            );
    }

    public onActionDone($event: any) {

        console.log('onActionDone', $event);

        switch ($event.do) {
            case 'delete':
                this.removeRow($event.doParams, $event.apipath);
                break;
            case 'reload':
                this.loadData();
                break;
            case 'navigate':
                this.router.navigate([$event.doParams.url]);
                break;
        }
    }

    public onActionError(err: any) {
        this.toastr.error(err.message, 'Errore ' + err.status, {timeOut: 10000});
        console.log('Error occured', err);
    }

    private resetPage() {
        // console.log('resetPage');
        this.offset_current = 0;
        this.filterQuery = {
            page_size: 15,
            page: 1
        };
    }

  public exportCSV() {
    this.downloadService.downloadFile(this.rows, this.TPLoptions.csv_export_opts);
  }
}

/*updateValue(event, cell, rowIndex) {
    console.log('inline editing rowIndex', rowIndex);
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    // this.rows = [...this.rows];
    console.log('UPDATED!', this.rows[rowIndex][cell]);
}*/
