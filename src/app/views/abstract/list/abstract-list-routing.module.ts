import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AbstractListComponent} from './abstract-list.component';
import {AuthGuard} from '../../../_guards/auth.guard';

const routes: Routes = [
    {
        path: '', component: AbstractListComponent,    data: {title: ''}
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AbstractListRoutingModule {
}
