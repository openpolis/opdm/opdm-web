import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from '../../_guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./list/abstract-list.module').then(m => m.AbstractListModule),
        data: {
            title: '-'
        }
    },
    {
        path: 'edit',
        loadChildren: () => import('./edit/abstract-edit.module').then(m => m.AbstractEditModule),
        canActivate: [AuthGuard],
        data: {
            title: '-'
        }
    },
    {
        path: 'edit/:id',
        loadChildren: () => import('./edit/abstract-edit.module').then(m => m.AbstractEditModule),
        canActivate: [AuthGuard],
        data: {
            title: '-'
        }
    },
    {
        path: ':action',
        loadChildren: () => import('./list/abstract-list.module').then(m => m.AbstractListModule),
        data: {
            title: '-'
        }
    }
/*    {
        path: ':action',
        loadChildren: './list/abstract-list.module#AbstractListModule',
        data: {
            title: '-'
        }
    }*/
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AbstractRoutingModule {
}
