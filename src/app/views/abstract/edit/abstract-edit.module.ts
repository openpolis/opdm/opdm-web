// ANGULAR MODULES
import {NgModule, LOCALE_ID} from '@angular/core';

// VENDOR MODULES
// import {FormlyModule} from '@ngx-formly/core';

// APP MODULES
import {AppCommonSharedModule} from '../../../shared/app-common-shared.module';

// APP COMPONENTS
import {AbstractEditComponent} from './abstract-edit.component';

// ROUTING
import {AbstractEditRoutingModule} from './abstract-edit-routing.module';

//
@NgModule({
    imports: [
        AbstractEditRoutingModule,
        AppCommonSharedModule,
        // FormlyModule
    ],
    declarations: [
        AbstractEditComponent
    ],
    providers: [
        {provide: LOCALE_ID, useValue: 'it-IT'}
    ]
})
export class AbstractEditModule {
}
