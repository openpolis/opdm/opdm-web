// ANGULAR MODULES
import {ChangeDetectorRef, Component, OnInit, OnDestroy, TemplateRef} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute, Router} from '@angular/router';
import {DatePipe} from '@angular/common';

// RXJS MODULES
import {distinctUntilChanged, takeWhile, debounceTime} from 'rxjs/operators';

// VENDOR MODULES
import {FormlyFormOptions, FormlyFieldConfig} from '@ngx-formly/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import set from 'lodash-es/set';
import get from 'lodash-es/get';

// SERVICES
import {APICommonService} from '../../../_services/api-common.service';
import {ToastrService} from 'ngx-toastr';

// MODELS
import {Area, Membership, Organization, Person, Post} from '../../../models/';
import {APISerializer} from '../../../serializers/generic';

// CONFIG
import {AppConfig} from '../../../app.config';
import merge from 'lodash-es/merge';
import {forkJoin} from 'rxjs';


@Component({
    templateUrl: './abstract-edit.component.html'
})
export class AbstractEditComponent implements OnInit, OnDestroy {

    private alive = true;
    public showForm = false;

    public apipath: string;
    public id: number = null;
    public mode: string;
    public isSubmiting = false;
    public form = new FormGroup({});
    public options: FormlyFormOptions = {
        formState: {
            model: this.model
        }
    };

    public API_models = {
        areas: Area,
        persons: Person,
        organizations: Organization,
        memberships: Membership,
        posts: Post
    };

    // SHADOW MODEL PROPERTY + GET/SET
    private _model: any;

    // SHADOW TITLE PROPERTY + GET/SET
    private _title = '';

    public fields: FormlyFieldConfig[] = [];

    private modalRef: BsModalRef;

    set model(value: any) {
        // console.log('set model', value);
        value.ACTIONBUS = [];
        this._model = value;
        this.options.formState.model = {...this.model};
    };

    get model(): any {
        return this._model;
    }

    set title(value: string) {
        this._title = value;
        this.route.snapshot.parent.data.title = value;             // set parent router title to apipath label
        this.titleService.setTitle(value + ' - ' + this.config.appName);  // set HTML page title
    }

    get title(): string {
        return this._title;
    }



    constructor(
        private apiService: APICommonService,
        private cd: ChangeDetectorRef,
        private datePipe: DatePipe,
        private titleService: Title,
        private route: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
        private modalService: BsModalService,
        public config: AppConfig
    ) {

        // console.log('AbstractEditComponent constructor');

        this.route.params
            .pipe(takeWhile(() => this.alive))
            .subscribe(params => {
                this.showForm = false;
                this.id = +params['id']; // (+) converts string 'id' to a number
                this.apipath = params['content_type'];
                this.mode = isNaN(this.id) ? 'create' : 'update';
                this.form = new FormGroup({});
                this.init();

                // // console.log(this.apipath, this.id);
            });
    }

    public ngOnInit() {

        this.form.valueChanges
            .pipe(
                takeWhile(() => this.alive),
                debounceTime(1000),
                // distinctUntilChanged((prev, curr) => isEqual(prev, curr))
            )
            .subscribe(data => this.onFormValueChange(data));

        // console.log('abstractEdit', this.form);
    }

    public ngOnDestroy() {
        this.alive = false;
    }

    private init() {

        switch (this.mode) {
            case 'create':
                this.model = {}; // new this.API_models[this.apipath]();
                this.preFillModel();
                this.configureTemplate();
                break;

            case 'update':
                this.apiService.getById(this.apipath, this.id)
                    .pipe(takeWhile(() => this.alive))
                    .subscribe(
                        response => {
                            // console.log('init update', response);
                            this.model = response;
                            this.configureTemplate();
                        }
                    );
                break;
        }
    }

    private configureTemplate() {
        const TPL = this.config.getFormTemplate(this.apipath, this.id, this.alive);
        this.title = TPL.title;
        this.fields = TPL.fields;
        // console.log('all the fields', this.fields);
        this.options = <FormlyFormOptions>merge(this.options, TPL.options);
        this.mayBeLoadSubModels(this.fields);

        // apply expressionProperty for disabled based on formState to all fields
        /*
                TPL.fields.forEach(field => {
                    if (!field.templateOptions.similarityCheck) {
                        field.hideExpression = 'formState.hidden';
                    }
                });
        */
/*        TPL.fields.forEach(field => {

            if (field.fieldGroup) {

                field.fieldGroup.forEach(fieldGroupItem => {
                    if (this.options.formState.similarity.fields.indexOf(fieldGroupItem.key) !== -1) {
                        this.form.get(fieldGroupItem.key).valueChanges.subscribe(value => {
                            // console.log( fieldGroupItem.key, value );
                        });
                    }
                })
            }
        });*/


    }

    private mayBeLoadSubModels(fields) {

        const dataRequests = [];
        const itemKeys = [];

        fields.forEach((item) => {
            if (item.templateOptions && item.templateOptions.datasource) {

                itemKeys.push(item.key);
                dataRequests.push( this.apiService.getByParams(item.templateOptions.datasource, {}) )
            }
        });

        // console.log(itemKeys, dataRequests);

        // no data request to wait for, show the form and exit
        if (!dataRequests.length) { this.showForm = true; return; }

        // wait all data requests to complete
        forkJoin(dataRequests)
            .pipe( takeWhile(() => this.alive) )
            .subscribe(
            (responses: any) => {

                // loop over the keys and responses
                for (let i = 0; i < itemKeys.length; i++) {

                    // assign results to the model where due
                    this.model[itemKeys[i]] = responses[i].results ? responses[i].results : responses[i];
                }

                // all complete, show the form
                this.showForm = true;
            },
            (err) => {
                console.log('Error occured', err);
            }
        );
    }

    private preFillModel() {
        // gets queryParams from get parameters and sets them to the model
        const Q = this.route.snapshot.queryParams;

        Object.keys(Q).map(e => {
            // console.log(e, Q[e]);

            if (/^\d+$/.test(Q[e])) {
              // CAST to Number
              set(this.model, e, Number(Q[e]) );
            } else {
              // LEFT AS IS
              set(this.model, e, Q[e] );
            }
        });
    }

    private onFormValueChange($event) {
        // console.log('onFormValueChange $event', $event);
        // console.log(this.model);
        // console.log(this._model);
        // console.log(this.form);
        const FS = this.options.formState;

        if (this.mode === 'create') {

            if (!this.form.invalid && FS.similarityCheck && Array.isArray(FS.similarityCheck.fields) && FS.similarityCheck.fields.length) {

                const queryParams = {};

                this.options.formState.similarityCheck.fields.forEach( param => {
                    queryParams[param] = $event[param];
                });

                // delete empty string and non string parameters
                Object.keys(queryParams).forEach((key) => {
                    switch (typeof queryParams[key]) {
                        case 'string':
                        case 'number':
                            if (queryParams[key] === '') { delete queryParams[key] }
                        break;
                        default:
                            delete queryParams[key];
                        break;
                    }
                });

                // query similarity API if there are parameters
                if (Object.keys(queryParams).length) {
                    // SIMILARITY CHECK
                    this.apiService.similarityCheck(this.apipath, queryParams);
                }
                // console.log(queryParams);
            }
        }
    }

    public onModelChange($event) {
        // console.log('onModelChange $event', $event);
    }

    /* FORM COPY */
    public onCopy(data: any) {
      this.router.navigate([this.apipath + '/edit'], {queryParams: this.getCreationQuery()});
    }

    private getCreationQuery() {
      // console.log('getCreationQuery', this.model, this.options);
      const Q = {};
      if (this.options && this.options.formState.supportsCopy && Array.isArray(this.options.formState.copyFields)) {

        this.options.formState.copyFields.forEach(e => {
          // console.log(e);
          if (this.model[e]) {
            Q[e] = get(this.model, e);
          }
        });
        // console.log(Q);
      }
      return Q;
    }

    /* FORM SUBMIT */
    public onSubmit(data: any) {

        if (this.form.invalid) {
            return;
        }

        this.isSubmiting = true;
        const serializer = new APISerializer(this.apipath, data);
        const submitData = serializer.serialize();

        submitData.id = this.id;

        switch (this.mode) {
            case 'create':
                this.apiService.create(this.apipath, submitData).subscribe(
                    res => {
                        this.toastr.success(null, 'Salvataggio OK');
                        this.router.navigate([this.apipath + '/edit/' + res.id]);
                        // console.log(res);

                    },
                    err => {
                        this.isSubmiting = false;
                    },
                    () => {
                        this.isSubmiting = false;
                    }
                );
                break;

            case 'update':
                this.apiService.update(this.apipath, submitData).subscribe(
                    res => {
                        this.form.markAsPristine();
                        this.toastr.success(null, 'Salvataggio OK');
                        // console.log(res);
                    },
                    err => {
                        this.isSubmiting = false;
                    },
                    () => {
                        this.isSubmiting = false;
                    }
                );
                break;
        }
    }

    public deleteAsk(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'modal-lg modal-dialog-centered' })
        );
    }

    public deleteConfirm() {
        this.modalRef.hide();
        this.delete();
    }

    public deleteDecline() {
        this.modalRef.hide();
    }

    public delete() {
        this.apiService.delete(this.apipath, this.id)
            .pipe(takeWhile(() => this.alive))
            .subscribe(
                (response: any) => {
                    this.toastr.success(null, 'Rimozione OK');
                    setTimeout(() => {
                        this.router.navigate([this.apipath]);
                    }, 1000);
                }
            );
    }

}
