import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AbstractEditComponent} from './abstract-edit.component';

const routes: Routes = [
    {path: '', component: AbstractEditComponent, data: {title: ':Crea nuova'} },
    {path: ':id', component: AbstractEditComponent, data: {title: ':id'} }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AbstractEditRoutingModule {
}
