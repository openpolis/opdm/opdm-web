import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';


// Routing
import {AbstractRoutingModule} from './abstract-routing.module';

@NgModule({
    imports: [
        AbstractRoutingModule
    ],
    declarations: [
    ]
})
export class AbstractModule {
}
