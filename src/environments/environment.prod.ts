export const environment = {
    production: true,
    API: {
        baseURL: 'https://service.opdm.openpolis.io/api-mappepotere/v1/',
        authURL: 'https://service.opdm.openpolis.io/api-token-auth/',
        authRefreshURL: 'https://service.opdm.openpolis.io/api-token-refresh/'
    }
};
