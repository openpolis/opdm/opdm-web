describe('Memberships test', { scrollBehavior: 'center' }, () => {
  beforeEach(() => {
    cy.login()
  });

  it('Adds memberships from Person page', () => {
    cy.visit('/#/persons');
    cy.get('[data-cy="search"]').clear();
    cy.get('[data-cy="search"]').type('Ettore Di Cesare');
    cy.get('datatable-body-row').contains('Ettore Di Cesare').find('.datatable-body-cell-label > a').click();
    cy.get('[data-cy="given_name"]').should('have.value', 'Ettore');
    cy.get('[data-cy="family_name"]').should('have.value', 'Di Cesare');
    cy.wait(1000);
    cy.get('#memberships-add-row').click();
    cy.get('ng-select[data-cy="organization"] input').click();
    cy.get('ng-select[data-cy="organization"] input').clear();
    cy.get('ng-select[data-cy="organization"] input').type('Giunta comunale di L\'Aquila');
    cy.get('.ng-select-bottom .ng-option [title="Giunta comunale di L\'Aquila"]').click();
    cy.get('ng-select[data-cy="post_role"] input').clear();
    cy.get('ng-select[data-cy="post_role"] input').type('Sindaco');
    cy.get('.ng-select-bottom .ng-option [title="Sindaco"]').click();
    cy.get('[data-cy="start_date"]').clear();
    cy.get('[data-cy="start_date"]').type('2022-01-01');
    cy.get('#memberships-panel-save-button').click();
    cy.contains('.toast-title', 'Salvataggio OK');
  });

  it('Removes memberships from Person page', () => {
    cy.visit('/#/persons');
    cy.get('[data-cy="search"]').clear();
    cy.get('[data-cy="search"]').type('Ettore Di Cesare');
    cy.get('datatable-body-row').contains('Ettore Di Cesare').find('.datatable-body-cell-label > a').click();
    cy.get('[data-cy="given_name"]').should('have.value', 'Ettore');
    cy.get('[data-cy="family_name"]').should('have.value', 'Di Cesare');
    cy.wait(1000);
    cy.get('[data-cy="Incarichi"] .ngx-datatable .datatable-body-cell-label')
      .contains('Sindaco')
      .parents('.datatable-row-group')
      .find('app-action-item-delete span[title="Elimina"]').click();
    cy.get('#item-delete-confirm-button').click();
    cy.contains('.toast-title', 'Rimozione OK');
  });

  it('Adds memberships from Memberships page', () => {
    cy.visit('/#/memberships');
    cy.get('ng-select[data-cy="person__id"] input').clear();
    cy.get('ng-select[data-cy="person__id"] input').type('Ettore Di Cesare');
    cy.get('.ng-select-bottom .ng-option [title^="Ettore Di Cesare"]').click();
    cy.wait(1000);
    cy.get('#memberships-create-button').click();
    cy.get('ng-select[data-cy="organization"] input').click();
    cy.get('ng-select[data-cy="organization"] input').clear();
    cy.get('ng-select[data-cy="organization"] input').type('Giunta comunale di L\'Aquila');
    cy.get('.ng-select-bottom .ng-option [title="Giunta comunale di L\'Aquila"]').click();
    cy.get('ng-select[data-cy="post_role"] input').clear();
    cy.get('ng-select[data-cy="post_role"] input').type('Sindaco');
    cy.get('.ng-select-bottom .ng-option [title="Sindaco"]').click();
    cy.get('[data-cy="start_date"]').clear();
    cy.get('[data-cy="start_date"]').type('2022-01-01');
    cy.get('#main-save-button').click();
    cy.contains('.toast-title', 'Salvataggio OK');
  });
});
