describe('Login/Logout test', () => {
  it('Login pages exists', () => {
    cy.visit('/');
    cy.contains('Inserisci le tue credenziali');
  });

  it('Rejects login with bad credentials', () => {
    cy.visit('/');
    cy.get('#username').type('123');
    cy.get('#password').type('123');
    cy.get('#login-button').click();
    cy.contains('.toast-title', '401');
  });

  it('Signs in with good credentials', () => {
    cy.visit('/');
    cy.get('#username').type(Cypress.env('username'));
    cy.get('#password').type(Cypress.env('password'));
    cy.get('#login-button').click().should(() => {
      expect(localStorage.getItem('OPDM_currentUser')).to.contains('{"token":');
    });
    cy.location().should((loc) => {
      expect(loc.hash).to.eq('#/dashboard')
    });
  });

  it('Logs out correctly', () => {
    cy.get('.app-header .icon-user').click();
    cy.get('.app-header .fa-sign-out-alt').click().should(() => {
      expect(localStorage.getItem('OPDM_currentUser')).to.be.null;
    });
    cy.location().should((loc) => {
      expect(loc.hash).to.eq('#/pages/login')
    });
  });
});
