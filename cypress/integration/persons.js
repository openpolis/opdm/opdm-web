describe('Persons test', () => {
  beforeEach(() => {
    cy.login()
  });

  it('Navigates to Persona page', () => {
    cy.visit('/#/persons');
  });

  it('Filters results', () => {
    cy.visit('/#/persons');
    cy.get('[data-cy="search"]').clear();
    cy.get('[data-cy="search"]').type('Ettore Di Cesare');
    cy.get('.ngx-datatable :nth-child(4) > .datatable-body-cell-label').contains('Ettore');
    cy.get('.ngx-datatable :nth-child(5) > .datatable-body-cell-label').contains('Di Cesare');
  })

  it('Creates new person', () => {
    cy.visit('/#/persons/edit');
    cy.get('[data-cy="given_name"]').clear();
    cy.get('[data-cy="given_name"]').type('NuovaPersonaNome');
    cy.get('[data-cy="family_name"]').clear();
    cy.get('[data-cy="family_name"]').type('NuovaPersonaCognome');
    cy.get('ng-select[data-cy="birth_location_area"] input').clear();
    cy.get('ng-select[data-cy="birth_location_area"] input').type('Roma');
    cy.get('.ng-select-bottom .highlighted').contains(/^Roma$/).click();
    cy.get('[data-cy="birth_date"]').clear();
    cy.get('[data-cy="birth_date"]').type('2021-11-21');
    cy.get(':nth-child(1) > .custom-control-label').click();
    cy.get(':nth-child(1) > [data-cy="gender"]').check();
    cy.get('#main-save-button').click();
    cy.contains('.toast-title', 'Salvataggio OK');
  });

  it('Checks person similarity', () => {
    cy.visit('/#/persons/edit');
    cy.get('[data-cy="given_name"]').clear();
    cy.get('[data-cy="given_name"]').type('NuovaPersonaNom');
    cy.get('[data-cy="family_name"]').clear();
    cy.get('[data-cy="family_name"]').type('NuovaPersonaCognom');
    cy.get('[data-cy="Similarità trovate"]').contains('NuovaPersonaNom');
    cy.get('[data-cy="Similarità trovate"]').contains('NuovaPersonaCognom');
  });

  it('Deletes person', () => {
    cy.visit('/#/persons');
    cy.get('[data-cy="search"]').clear();
    cy.get('[data-cy="search"]').type('NuovaPersonaNom NuovaPersonaCognom');
    cy.get('.ngx-datatable .datatable-body-cell-label').contains('NuovaPersonaNom');
    cy.get('.ngx-datatable .datatable-body-cell-label')
      .contains('NuovaPersonaNom')
      .parents('.datatable-row-group')
      .find('app-action-item-delete span[title="Elimina"]').click();
    cy.get('#item-delete-confirm-button').click();
    cy.wait(1000);
    cy.get('body').click();
    cy.contains('.toast-title', 'Rimozione OK');
  })

});
